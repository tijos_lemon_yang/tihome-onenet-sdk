import onenet.Signature
from onenet.RestApiRequest import RestApiRequest 

class HttpApi:    
    url =  'https://iot-api.heclouds.com'
    authToken = ''
    httpRequest = RestApiRequest()
    def __init__(self, userId, accessKey) -> None:        
        self.authToken = onenet.Signature.createToken(accessKey=accessKey, userId=userId)
        print(self.authToken)

    #设备管理类
    def createDevice(self, productId : str, deviceName : str, desc : str):
        
        # 创建设备
        # @param {string} productId 
        # @param {string} deviceName 
        # @param {string} desc 
        # @returns        
        body = {
            'product_id' : productId,
            'device_name' : deviceName,
            'desc' : desc
            }

        return  self.httpRequest.post(url=self.url, authorization=self.authToken, path='device', action='create', body=body)

    def deleteDevice(self, productId : str, deviceName : str):
       # 删除设备
       # @param {string} productId 
       # @param {string} deviceName 
       # @returns 
       
       body = {
            'product_id' : productId,
            'device_name' : deviceName,           
       }
       return self.httpRequest.post(url=self.url, authorization=self.authToken, path='device', action='delete', body=body)
 
    def queryDeviceDetail(self, productId : str, deviceName : str):
        # 查询设备信息
        # @param {string} productId 
        # @param {string} deviceName 
        # @returns 
        params = {
        'product_id' : productId,
        'device_name' : deviceName,
        }

        return self.httpRequest.get(url = self.url, authorization = self.authToken, path='device', action='detail', params =params)

    def moveDevice(self, productId : str, targetUser : str, deviceNames:list):
        #设备转移
        #
        body = {
        'product_id' : productId,
        'target_user_tel' : targetUser,    
        'device_name' : deviceNames       
        }
        
        return self.httpRequest.post(url=self.url, authorization=self.authToken, path='device', action='movedevice', body=body)


    def groupAddDevice(self, groupId : str, productId : str, deviceNames : list):
        # 将设备加入到组
        # @param {string} groupId 
        # @param {string} productId 
        # @param {string} deviceNames  multiple device list 
        # @returns 
                
        body = {
                'product_id' : productId,
                'device_name_list' : deviceNames,        
                'group_id' : groupId
        }
        
        return self.httpRequest.post(url = self.url, 
                                    authorization = self.authToken, 
                                    path='devicegroup', 
                                    action='add-devices', 
                                    body = body)
    
    def groupRemoveDevice(self, groupId : str, productId : str, deviceNames : list):
        # 从组中删除设备
        # @param {string} groupId 
        # @param {string} productId 
        # @param {string} deviceNames multiple device list
        # @returns 
        
        body = {
                'product_id' : productId,
                'device_name_list' : deviceNames,           
                'group_id' : groupId
        }
        
        return self.httpRequest.post(url = self.url, 
                                    authorization = self.authToken, 
                                    path='devicegroup', 
                                    action='del-devices', 
                                    body = body)
    
    def setDeviceProperty(self, productId : str, deviceName : str, propertyNameValues:dict):
        #  设置设备属性值 
        # @param {string} productId 
        # @param {string} deviceName 
        # @param {string} propertyName 
        # @param {object} propertyValue 
        # @returns            
        body = {
            'product_id' : productId,
            'device_name' : deviceName, 
            'params':  propertyNameValues
        }
        return self.httpRequest.post(url=self.url, authorization=self.authToken, path='thingmodel', action='set-device-property', body=body)
 
    def queryDeviceProperty(self, productId : str, deviceName :str, propertyNames : list):
        # 查询设备属性值
        # @param {string} productId 
        # @param {string} deviceName 
        # @param {string} propertyName 
        # @returns  
        body = {
            'product_id' : productId,
            'device_name' : deviceName,           
            'params' :propertyNames
        }
        return self.httpRequest.post(url=self.url, authorization=self.authToken, path='thingmodel', action='query-device-property-detail', body=body)

 
    def callDeviceService(self, productId : str, deviceName : str, serviceName : str, serviceParams : dict):
        # 调用设备服务
        # @param {string} productId 
        # @param {string} deviceName 
        # @param {string} serviceName 
        # @param {Map} serviceParams 
        # @returns 
        body = {
            'product_id' : productId,
            'device_name' : deviceName,           
            'identifier' : serviceName,
            'params' : serviceParams
        }
        return self.httpRequest.post(url=self.url, authorization=self.authToken, path='thingmodel', action='call-service', body=body)
