# OneNET物联网开放平台应用接入SDK(JAVA)

本文档提供了OneNET物联网开放平台应用接入API、例程。

注意：Onet物联网开放平台与OneNET stuido使用方式相同， 但平台API有区别。

本SDK封装了对接过程最基本的接口API，可以满足大部分的对接需求， 如果您有其它API需求，可以联系我们进行填加， 也可直接修改源码进行处理。 

本SDK支持两种API:  TiHome 封装API和平台HTTP API

TiHome封装API基于平台HTTP API进行了二次封装， 更易于理解， 与OneNET物联网开放平台中的物模型相对应； 平台HTTP API主要封装了平台的HTTP 请求和响应， 更靠近底层； 建议用户使用TiHome 封装API， 同时SDK中也提供了相应的源码供参考和修改。



**本SDK基于中国移动OneNET Studio SDK基础上进行修改以适应OneNET物联网开放平台, 原始代码可参考：**

[cm-heclouds/onenet-studio-api-java-sdk: onenet-studio-api-java-sdk (github.com)](https://github.com/cm-heclouds/onenet-studio-api-java-sdk)



## 文件说明

| 文件                                   | 说明               |      |
|--------------------------------------| ------------------ | ---- |
| onenet-iot-api-sdk.jar               | 接口API            |      |
| onenet-iot-api-sdk-sources.jar | 源码               |      |
| onenet-iot-api-sdk-javadoc     | 生成的JavaDoc 文档 |      |

## 准备工作

### 获取userid和access key 

该参数可从OneNET账号安全设置中获取， 如下所示：

![image-20230525190047111](./img/image-20230525190047111.png)

![image-20230525190143521](./img/image-20230525190143521.png)

## TiHome 封装API

为了方便用户对设备进行操作， 基于平台HTTP API对设备功能进行了次封装， 可对设备功能进行直接操作。 如果该类不满足要求，可联系我们增加其它API，可基于下方HTTP API进行封装。

![image-20230602110301113](./img/image-20230602110301113.png)

### TiHome API说明

相关API及功能如下表所示：

| 功能       | 说明             | 限定符和类型    | 方法和说明                                          |
| ---------- | ---------------- | --------------- | --------------------------------------------------- |
| 设备管理   | 获取设备状态     | Status          | getDeviceStatus()                                   |
|            | 将设备加入到分组 | void            | addToGroup(String groupId)                          |
|            | 从分组中删除设备 | void            | removeFromGroup(String groupId)                     |
| 设备操作   | 获取音量大小     | int             | getAudioVolume()                                    |
|            | 设置音量大小     | void            | setAudioVolume(int volume)                          |
|            | 文字转语音播放   | String          | ttsPlay(String text)                                |
|            | 重启设备         | void            | reboot()                                            |
| 联系人操作 | 设置紧急联系人   | void            | setUrgentContacts(List<String> phoneNumbers)        |
|            | 设备亲情联系人   | void            | setFamilyNumbers(List<FamilyNumber> numbers)        |
|            | 远程呼叫         | String          | remoteCall(String phone, String text)               |
| 语音留言   | 语音留言         | String          | leaveMessageAdd(String mid, String url, int format) |
| 服务打卡   | 开始服务打卡     | void            | servicePunchStart(int seconds)                      |
|            | 完成打卡         | int             | servicePunchFinish(String code)                     |
| 子设备管理 | 获取子设备列表   | List<SubDevice> | getSubDevices()                                     |
|            | 添加子设备       | void            | addSubDevice(SubDevice subDevice)                   |
|            | 修改子设备       | void            | changeSubDevice(SubDevice subDevice)                |
|            | 删除子设备       | void            | deleteSubDevice(String deviceSN)                    |
|            |                  |                 |                                                     |

例程代码请参考： \src\test\java\test\tihome\TiHomeTest.java

![image-20230602112701605](./img/image-20230602112701605.png)





## 平台HTTP API说明

在应用中使用包名 com.tigear.api下的接口API为平台HTTP API的封装， 主要由设备管理API、设备属性设置获取API、设备服务调用API组成

![image-20230525202325664](./img/image-20230525202325664.png)



### DeviceManagerApi 设备管理API 

用于进行设备管理，包括创建、删除、获取信息、将设备加到分组，由于设备是通过转移动您的账号下的，不需要创建，只需要通过设备名称获取设备信息即可。

![image-20230526095758609](./img/image-20230526095758609.png)

### DevicePropertyApi 设备属性设置获取API 

用于设置和获取设备的属性， 支持对单个或多个属性进行操作

![image-20230525202826938](./img/image-20230525202826938.png)

### DeviceServiceApi 设备服务调用API

用于提高设备指定的服务， 同步调用的服务直接返回执行结果， 异步调用的服务需要通过HTTP数据推送来得到结果

![image-20230525202930236](./img/image-20230525202930236.png)





## 测试工程

通过IDEA打开例子工程， 可在test中找到相应的测试代码，通过单独运行测试程序中的函数来查看, 在运行之前先将userid, accesskey设置成您账号的配置。

### 设置userId, accessKey

![image-20230525192449734](./img/image-20230525192449734.png)

### 运行测试用例

![image-20230525190322871](./img/image-20230525190322871.png)