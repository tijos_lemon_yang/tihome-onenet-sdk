package com.tigear.api;

import cm.heclouds.onenet.iot.api.IotClient;
import cm.heclouds.onenet.iot.api.IotProfile;
import cm.heclouds.onenet.iot.api.auth.SignatureMethod;

import java.io.IOException;

/**
 * 初始化
 */
public class OneNetIotClient {
    IotClient client;

    /**
     * 初始化
     * @param userId    OneNET平台账号 userId
     * @param accessKey  OneNET平台账号 accessKey
     */
    public void initClient(String userId, String accessKey) {
        IotProfile profile = new IotProfile();
        profile
                .userId(userId)
                .accessKey(accessKey)
                .signatureMethod(SignatureMethod.SHA256);
        client = IotClient.create(profile);
    }

    /**
     * 关闭并释放资源
     * @throws IOException 如果有IO 错误
     */
    public void destroyClient() throws IOException {
        client.close();
    }

    public IotClient getClient(){ return this.client;}

}
