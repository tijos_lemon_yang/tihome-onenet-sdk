package com.tigear.api;

import cm.heclouds.onenet.iot.api.entity.application.device.*;
import cm.heclouds.onenet.iot.api.entity.application.group.*;
import cm.heclouds.onenet.iot.api.exception.IotClientException;
import cm.heclouds.onenet.iot.api.exception.IotServerException;
import com.alibaba.fastjson.JSON;

import java.util.Collections;
import java.util.List;

/**
 * 设备管理API
 */
public class DeviceManagerApi {
    OneNetIotClient iotClient;
    public DeviceManagerApi(OneNetIotClient iotClient) {
        this.iotClient = iotClient;
    }

    /**
     * 创建新设备
     * @param productId   产品ID
     * @param deviceName    设备名称
     * @param description   设备描述
     * @throws IotServerException
     * @throws IotClientException
     */
    public void createDevice(String productId, String deviceName, String description) throws IotServerException, IotClientException {
        CreateDeviceRequest request = new CreateDeviceRequest();
        request.setProductId(productId);
        request.setDeviceName(deviceName);
        request.setDesc(description);

        CreateDeviceResponse response = iotClient.getClient().sendRequest(request);
        return ;
    }

    /**
     * 更新设备描述
     * @param productId   产品ID
     * @param deviceName    设备名称
     * @param description   设备描述
     * @throws IotServerException
     * @throws IotClientException
     */
    public void updateDevice(String productId, String deviceName, String description) throws IotServerException, IotClientException {
        UpdateDeviceRequest request = new UpdateDeviceRequest();
        request.setProductId(productId);
        request.setDeviceName(deviceName);
        request.setDesc(description);

        UpdateDeviceResponse response = iotClient.getClient().sendRequest(request);
        return ;
    }

    /**
     * 删除设备
     * @param productId   产品ID
     * @param deviceName    设备名称
     * @throws IotServerException
     * @throws IotClientException
     */
    public void deleteDevice(String productId, String deviceName) throws IotServerException, IotClientException {
        DeleteDeviceRequest request = new DeleteDeviceRequest();
        request.setProductId(productId);
        request.setDeviceName(deviceName);

        DeleteDeviceResponse response = iotClient.getClient().sendRequest(request);
        return ;
    }

    /**
     * 获取设备详情
     * @param productId   产品ID
     * @param deviceName    设备名称
     * @return
     * @throws IotServerException
     * @throws IotClientException
     */
    public QueryDeviceDetailResponse queryDeviceDetail(String productId, String deviceName) throws IotServerException, IotClientException {
        QueryDeviceDetailRequest request = new QueryDeviceDetailRequest();
        request.setProductId(productId);
        request.setDeviceName(deviceName);

        QueryDeviceDetailResponse response = iotClient.getClient().sendRequest(request);
        return response;
    }

    /**
     * 获取分组详情
     * @param groupId
     * @return
     * @throws IotServerException
     * @throws IotClientException
     */
    public QueryGroupDetailResponse queryGroupDetail(String groupId) throws IotServerException, IotClientException{
        QueryGroupDetailRequest request = new QueryGroupDetailRequest();
        request.setId(groupId);

        QueryGroupDetailResponse response = iotClient.getClient().sendRequest(request);
        return response;
    }

    /**
     * 获取分组下的设备列表
     * @param groupId
     * @return
     * @throws IotServerException
     * @throws IotClientException
     */
    public QueryGroupDeviceListResponse queryGroupDeviceList(String groupId, int offset, int limit) throws  IotServerException, IotClientException{
        QueryGroupDeviceListRequest request = new QueryGroupDeviceListRequest();
        request.setId(groupId);
        request.setOffset(offset);
        request.setLimit(limit);

        QueryGroupDeviceListResponse response = iotClient.getClient().sendRequest(request);
        return response;
    }


    /**
     * 将设备加到分组
     * @param groupId   分组ID
     * @param productId  产品名称
     * @param deviceNames  设备列表
     * @return  设备添加结果
     * @throws IotServerException
     * @throws IotClientException
     */
    public AddGroupDeviceResponse addGroupDevice(String groupId, String productId, List<String> deviceNames) throws IotServerException, IotClientException {
        AddGroupDeviceRequest request = new AddGroupDeviceRequest();
        request.setId(groupId);
        request.setProductId(productId);
        request.setDevices(deviceNames);

        AddGroupDeviceResponse response = iotClient.getClient().sendRequest(request);
        return response;
    }

    /**
     * 从分组删除设备
     * @param groupId   分组ID
     * @param productId  产品名称
     * @param deviceNames  设备列表
     * @return  设备删除结果
     * @throws IotServerException
     * @throws IotClientException
     */
    public RemoveGroupDeviceResponse removeGroupDevice(String groupId, String productId, List<String> deviceNames) throws IotServerException, IotClientException {
        RemoveGroupDeviceRequest request = new RemoveGroupDeviceRequest();
        request.setId(groupId);
        request.setProductId(productId);
        request.setDevices(deviceNames);

        RemoveGroupDeviceResponse response = iotClient.getClient().sendRequest(request);
        return response;
    }
}
