package com.tigear.api;

import cm.heclouds.onenet.iot.api.entity.application.device.*;
import cm.heclouds.onenet.iot.api.exception.IotClientException;
import cm.heclouds.onenet.iot.api.exception.IotServerException;
import com.alibaba.fastjson.JSON;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 设备属性设置获取API
 */
public class DevicePropertyApi {

    OneNetIotClient iotClient;
    public DevicePropertyApi(OneNetIotClient iotClient) {
        this.iotClient = iotClient;
    }

    /**
     * 设置设备属性
     * @param productId   产品ID
     * @param deviceName    设备名称
     * @param propertyId  属性标识
     * @param value  属性值
     * @return
     * @throws IotServerException
     * @throws IotClientException
     */
    public SetDevicePropertyResponse setDeviceProperty(String productId, String deviceName, String propertyId, Object value) throws IotServerException, IotClientException {
        SetDevicePropertyRequest request = new SetDevicePropertyRequest();
        request.setProductId(productId);
        request.setDeviceName(deviceName);
        request.addParam(propertyId, value);

        SetDevicePropertyResponse response = iotClient.getClient().sendRequest(request);
        return response;
    }

    /**
     * 批量设置设备属性
     * @param productId   产品ID
     * @param deviceName    设备名称
     * @param propertyValues  属性名称，值集合
     * @return
     * @throws IotServerException
     * @throws IotClientException
     */
    public SetDevicePropertyResponse setDeviceProperty(String productId, String deviceName, Map<String, Object> propertyValues) throws IotServerException, IotClientException {
        SetDevicePropertyRequest request = new SetDevicePropertyRequest();
        request.setProductId(productId);
        request.setDeviceName(deviceName);

        propertyValues.forEach((key, value)->{
            request.addParam(key, value);
        });

        SetDevicePropertyResponse response = iotClient.getClient().sendRequest(request);
        return response;
    }

    /**
     * 获取设备属性值
     * @param productId   产品ID
     * @param deviceName    设备名称
     * @param propertyIds  属性名称集合
     * @throws IotServerException
     * @throws IotClientException
     */
    public HashMap<String, Object> queryDevicePropertyValues(String productId, String deviceName, String [] propertyIds) throws IotServerException, IotClientException {
        QueryDevicePropertyDetailRequest request = new QueryDevicePropertyDetailRequest();
        request.setProductId(productId);
        request.setDeviceName(deviceName);
        request.setPropertyIds(propertyIds);

        QueryDevicePropertyDetailResponse response = iotClient.getClient().sendRequest(request);
        return response;
    }

    public  SetDeviceDesiredPropertyResponse setDeviceDesiredProperty(String productId, String deviceName, String propertyId, Object value) throws IotServerException, IotClientException {
        SetDeviceDesiredPropertyRequest request = new SetDeviceDesiredPropertyRequest();
        request.setProductId(productId);
        request.setDeviceName(deviceName);

         request.addParam(propertyId, value);
        SetDeviceDesiredPropertyResponse response = iotClient.getClient().sendRequest(request);
        return response;
    }

    public  SetDeviceDesiredPropertyResponse setDeviceDesiredProperty(String productId, String deviceName, Map<String, Object> propertyValues) throws IotServerException, IotClientException {
        SetDeviceDesiredPropertyRequest request = new SetDeviceDesiredPropertyRequest();
        request.setProductId(productId);
        request.setDeviceName(deviceName);

        propertyValues.forEach((key, value) ->{
            request.addParam(key, value);
        });

        SetDeviceDesiredPropertyResponse response = iotClient.getClient().sendRequest(request);
        return response;
    }

    public  QueryDeviceDesiredPropertyResponse queryDeviceDesiredPropertyValues(String productId, String deviceName, List<String> propertyIds) throws IotServerException, IotClientException {
        QueryDeviceDesiredPropertyRequest request = new QueryDeviceDesiredPropertyRequest();
        request.setProductId(productId);
        request.setDeviceName(deviceName);
        request.setParams(propertyIds);

        QueryDeviceDesiredPropertyResponse response = iotClient.getClient().sendRequest(request);
        return response;
    }

    public  DeleteDeviceDesiredPropertyResponse deleteDeviceDesiredProperty(String productId, String deviceName, List<String> propertyIds) throws IotServerException, IotClientException {
        DeleteDeviceDesiredPropertyRequest request = new DeleteDeviceDesiredPropertyRequest();
        request.setProductId(productId);
        request.setDeviceName(deviceName);

        propertyIds.forEach((value)->{
            request.addParam(value);
        });

        DeleteDeviceDesiredPropertyResponse response = iotClient.getClient().sendRequest(request);
        return response;
    }

}
