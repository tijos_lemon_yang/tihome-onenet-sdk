package com.tigear.api;

import cm.heclouds.onenet.iot.api.entity.application.device.CallServiceRequest;
import cm.heclouds.onenet.iot.api.entity.application.device.CallServiceResponse;
import cm.heclouds.onenet.iot.api.exception.IotClientException;
import cm.heclouds.onenet.iot.api.exception.IotServerException;
import com.alibaba.fastjson.JSON;

import java.util.HashMap;
import java.util.Map;

/**
 * 设备服务调用API
 */
public class DeviceServiceApi {

    OneNetIotClient iotClient;
    public DeviceServiceApi(OneNetIotClient iotClient) {
        this.iotClient = iotClient;
    }

    /**
     * 调用设备服务
     * @param productId   产品ID
     * @param deviceName    设备名称
     * @param serviceId     服务ID
     * @param serviceParams  服务参数
     * @return
     * @throws IotServerException
     * @throws IotClientException
     */
    public CallServiceResponse callService(String productId, String deviceName, String serviceId, Map<String, Object> serviceParams) throws IotServerException, IotClientException {
        CallServiceRequest request = new CallServiceRequest();
        request.setProductId(productId);
        request.setDeviceName(deviceName);
        request.setIdentifier(serviceId);

        request.setParams(serviceParams);

        CallServiceResponse response = iotClient.getClient().sendRequest(request);
        return response;
    }
}
