package com.tigear.tihome.type;

/**
 * 亲情联系人类型
 */
public enum PhoneType {

    /**
     * 无
     */
    None (0),
    /**
     * 社区
     */
    COMMUNITY(1),
    /**
     * 儿子
     */
    SON(2),
    /**
     * 女儿
     */
    DAUGHTER(3),
    /**
     * 护工
     */
    NURSE(4),
    /**
     * 机构
     */
    ORGNIZATION (5);

    private Integer value = 0;

    PhoneType(Integer value) {this.value = value;}

    public Integer getValue() {
        return value;
    }

    public  void setValue(Integer value) { this.value = value;};
}
