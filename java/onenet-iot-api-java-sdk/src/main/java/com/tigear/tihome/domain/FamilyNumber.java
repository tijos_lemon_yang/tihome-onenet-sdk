package com.tigear.tihome.domain;

import com.alibaba.fastjson.JSONObject;
import com.tigear.tihome.type.PhoneType;

/**
 * 亲情联系人
 */
public class FamilyNumber {
    /**
     * 电话号码
     */
    String phone = "";
    /**
     * 联系人类型
     */
    PhoneType type = PhoneType.None;

    public FamilyNumber(){
        this.phone = "";
        this.type = PhoneType.None;
    }

    public FamilyNumber(String phone, PhoneType type) {
        this.phone = phone;
        this.type = type;
    }

    public JSONObject toJsonObject(){
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("phone", this.phone);
        jsonObject.put("type", this.type.getValue());

        return jsonObject;
    }

    public void fromJsonObject(JSONObject jsonObject) {
        this.phone = jsonObject.getString("phone");
        this.type.setValue(jsonObject.getInteger("type"));
    }
}
