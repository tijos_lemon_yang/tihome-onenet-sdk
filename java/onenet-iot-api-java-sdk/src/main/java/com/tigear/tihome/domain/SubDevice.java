package com.tigear.tihome.domain;

import com.alibaba.fastjson.JSONObject;
import com.tigear.tihome.type.ActionType;
import com.tigear.tihome.type.DeviceType;

/**
 * 子设备
 */
public class SubDevice {
    /**
     * 子设备ID
     */
    String deviceSN;
    /**
     * 设备类型
     */
    DeviceType deviceType = DeviceType.SOSButton;
    /**
     * 报警动作类型
     */
    ActionType action = ActionType.NONE;
    /**
     * 报警动作关联参数值  仅拔打亲情联系人时有效，用于指定亲情联系人ID
     */
    int actionParam;

    /**
     * 子设备发生报警时，网关是否提示， 报警提示方式  0 静音  1 声音提示
     *
     */
    int alarmAudible; // 0 silence  1 audible


    public SubDevice(String deviceSN, DeviceType deviceType, ActionType action, int actionParam, int alarmAudible) {
        this.deviceSN = deviceSN;
        this.deviceType = deviceType;
        this.action = action;
        this.actionParam = actionParam;
        this.alarmAudible = alarmAudible;
    }

    public SubDevice(JSONObject jsonObject) {
        this.fromJsonObject(jsonObject);
    }

    public JSONObject toJsonObject(){
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("uid", this.deviceSN);
        jsonObject.put("devtype", this.deviceType.getValue());
        jsonObject.put("action", this.action.getValue());
        jsonObject.put("param", this.actionParam);
        jsonObject.put("audible", this.alarmAudible);

        return jsonObject;
    }

    public  void fromJsonObject(JSONObject jsonObject){
        this.deviceSN = jsonObject.getString("uid");
        this.deviceType.setValue(jsonObject.getInteger("devtype"));
        this.action.setValue(jsonObject.getInteger("action"));
        this.actionParam = jsonObject.getInteger("param");
        this.alarmAudible = jsonObject.getInteger("audible");
    }
}
