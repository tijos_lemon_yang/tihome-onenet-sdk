package com.tigear.tihome.type;

/**
 * 子设备类型
 */
public enum DeviceType {

    /**
     * 无线按钮或一键报警器
     */
    SOSButton (16),

    /**
     * 烟感
     */
    FireAlarm(17),

    /**
     * 门磁
     */
    DoorAlarm(18),

    /**
     * 水浸
     */
    WaterAlarm(19),

    /**
     * 人体感应
     */
    BodyAlarm(20),

    /**
     * 燃气报警器
     */
    GasAlarm (21);

    private Integer value = 0;

    DeviceType(Integer value) {this.value = value;}

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {this.value = value;}
}
