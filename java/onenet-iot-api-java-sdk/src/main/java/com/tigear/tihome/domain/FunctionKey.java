package com.tigear.tihome.domain;

import com.alibaba.fastjson.JSONObject;
import com.tigear.tihome.type.FunctionType;

/**
 * 功能键定义
 */
public class FunctionKey {
    /**
     * 功能
     */
    FunctionType  functionType;
    /**
     * 相关参数
     */
    int  param;

    public FunctionKey(FunctionType functionType,  int param) {
        this.functionType = functionType;
        this.param = param;
    }

    public FunctionType getFunctionType() {
        return functionType;
    }

    public int getParam() {
        return param;
    }

    public JSONObject toJsonObject(){
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("function", functionType.getValue());
        jsonObject.put("param", this.param);

        return jsonObject;
    }


}
