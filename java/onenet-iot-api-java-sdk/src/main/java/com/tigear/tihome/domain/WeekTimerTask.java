package com.tigear.tihome.domain;

import com.alibaba.fastjson.JSONObject;

/**
 * 定时任务
 */
public class WeekTimerTask {
    /**
     * 小时
     */
    int hour;
    /**
     * 分钟
     */
    int min;
    /**
     * 星期
     */
    int week;
    /**
     * 文字base64
     */
    String text;
    /**
     * 禁用   0 启用  1 禁用
     */
    int disable;
    /**
     * 在线音频
     */
    String url;
    /**
     * 播放次数
     */
    int times;

    public WeekTimerTask(){

    }

    public  WeekTimerTask(int hour, int min, int week, String textBase64, int disable, String url, int times){
        this.hour = hour;
        this.min = min;
        this.week = week;
        this.text = textBase64;
        this.disable =disable;
        this.url = url;
        this.times = times;
    }

    public JSONObject toJsonObject() {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("hour", hour);
        jsonObject.put("min", min);
        jsonObject.put("week", week);
        jsonObject.put("text", text);
        jsonObject.put("disable", disable);
        jsonObject.put("url", url);
        jsonObject.put("times", times);

        return jsonObject;
    }

    public  void fromJsonObject(JSONObject jsonObject){
        this.hour = jsonObject.getInteger("hour");
        this.min=jsonObject.getInteger("min");
        this.week = jsonObject.getInteger("week");
        this.text = jsonObject.getString("text");
        this.disable = jsonObject.getInteger("disable");
        this.url = jsonObject.getString("url");
        if(this.url == null) {
            this.url="";
        }
        this.times = jsonObject.getIntValue("times");
        if(this.times == 0) {
            this.times =3;
        }
    }


}
