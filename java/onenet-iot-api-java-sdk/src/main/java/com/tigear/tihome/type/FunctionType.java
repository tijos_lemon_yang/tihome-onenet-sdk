package com.tigear.tihome.type;

/**
 * 功能键功能定义
 * 默认拔打亲情联系人电话
 */
public enum FunctionType {
    /**
     * 拔打亲情联系人电话
     */
    CALL_FAMILY(0),
    /**
     * 播放留言
     */
    PLAY_MESSAGE(1),
    /**
     * 挂断电话
     */
    END_CALL(2);

    private Integer value = 0;

    FunctionType(Integer value) {this.value = value;}

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {this.value = value;}
}
