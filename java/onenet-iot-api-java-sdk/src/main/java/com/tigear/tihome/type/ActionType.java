package com.tigear.tihome.type;

/**
 * 子设备报警动作类型
 */
public enum ActionType {
    /**
     * 无
     */
    NONE(0),
    /**
     * 拔打紧急电话
     */
    CALL_URGENT(1),
    /**
     * 拔打亲情联系人电话
     */
    CALL_FAMILY(2),
    /**
     * 播放留言
     */
    PLAY_MESSAGE(3);

    private Integer value = 0;

    ActionType(Integer value) {this.value = value;}

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {this.value = value;}

}
