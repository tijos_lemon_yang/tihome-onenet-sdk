package cm.heclouds.onenet.iot.api.entity.application.device;

import cm.heclouds.onenet.iot.api.AbstractResponse;

/**
 * 设备属性期望设置响应
 */
public class SetDeviceDesiredPropertyResponse extends AbstractResponse {

}
