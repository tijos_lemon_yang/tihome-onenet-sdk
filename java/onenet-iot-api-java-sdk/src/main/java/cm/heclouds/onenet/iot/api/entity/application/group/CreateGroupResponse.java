package cm.heclouds.onenet.iot.api.entity.application.group;

import cm.heclouds.onenet.iot.api.AbstractResponse;
import com.alibaba.fastjson.annotation.JSONField;

/**
 * 分组创建响应
 */
public class CreateGroupResponse extends AbstractResponse {

    /**
     * 分组ID
     */
    @JSONField(name = "group_id")
    private String groupId;

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }
}
