package cm.heclouds.onenet.iot.api.constant;

/**
 * @author ChengQi
 * @date 2020-07-03 9:33
 */
public class Constant {

    public static final String OPEN_API_URL = "http://iot-api.heclouds.com";

    public static final String OPEN_API_URL_SSL = "https://iot-api.heclouds.com";

    public static final String IOT_PROTOCOL_FORMAT = "iot.http.%d";

    public static final String OPEN_API_NAMESPACE_DEVICE = "device";

    public static final String OPEN_API_NAMESPACE_GROUP = "devicegroup";

    public static final String OPEN_API_NAMESPACE_THINGMODEL = "thingmodel";

    public static final String OPEN_API_SIGN_VERSION = "2022-05-01";

    public static final String DATE_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";

}
