package cm.heclouds.onenet.iot.api.entity.application.device;

import cm.heclouds.onenet.iot.api.AbstractResponse;

/**
 * 设备删除响应
 * <a href="https://open.iot.10086.cn/doc/iot_platform/book/api/common/deleteDevice.html">设备删除</a>
 * @author ChengQi
 * @date 2020-07-06 10:37
 */
public class DeleteDeviceResponse extends AbstractResponse {

}
