package cm.heclouds.onenet.iot.api.entity.common;

import com.alibaba.fastjson.annotation.JSONField;
import cm.heclouds.onenet.iot.api.entity.enums.FunctionEventType;
import cm.heclouds.onenet.iot.api.json.ValueHolderDeserializer;
import cm.heclouds.onenet.iot.api.json.ValueHolderSerializer;

import java.util.List;

/**
 * 功能点类型 事件类型
 * @author ChengQi
 * @date 2020/10/14
 */
public class Event extends AbstractFunction {

    /**
     * 事件类型
     */
    @JSONField(serializeUsing = ValueHolderSerializer.class, deserializeUsing = ValueHolderDeserializer.class)
    private FunctionEventType eventType;

    /**
     * 参数
     */
    private List<Parameter> outputData;

    public FunctionEventType getEventType() {
        return eventType;
    }

    public void setEventType(FunctionEventType eventType) {
        this.eventType = eventType;
    }

    public List<Parameter> getOutputData() {
        return outputData;
    }

    public void setOutputData(List<Parameter> outputData) {
        this.outputData = outputData;
    }
}
