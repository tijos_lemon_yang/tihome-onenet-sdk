package cm.heclouds.onenet.iot.api.entity.application.device;

import cm.heclouds.onenet.iot.api.entity.AbstractPagedResponse;

/**
 * 设备状态历史数据查询响应
 * <a href="https://open.iot.10086.cn/doc/iot_platform/book/api/application/queryDeviceStatusHistory.html">
 *     设备状态历史数据查询</a>
 * @author ChengQi
 * @date 2020-07-06 16:49
 */
public class QueryDeviceStatusHistoryResponse extends AbstractPagedResponse<DeviceStatus> {

}
