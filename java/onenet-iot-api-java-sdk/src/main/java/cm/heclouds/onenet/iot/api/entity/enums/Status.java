package cm.heclouds.onenet.iot.api.entity.enums;

import cm.heclouds.onenet.iot.api.json.ValueHolder;

/**
 * 设备状态 枚举类型
 * @author ChengQi
 * @date 2020-07-10 15:47
 */
public enum Status implements ValueHolder<Integer> {

    /**
     * 未激活
     */
    INACTIVE(2),
    /**
     * 在线
     */
    ONLINE(1),
    /**
     * 离线
     */
    OFFLINE(0);

    private final Integer value;

    Status(Integer value) {
        this.value = value;
    }

    @Override
    public Integer getValue() {
        return value;
    }
}
