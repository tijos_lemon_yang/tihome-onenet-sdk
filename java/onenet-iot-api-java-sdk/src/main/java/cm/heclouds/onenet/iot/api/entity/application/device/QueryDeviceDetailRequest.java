package cm.heclouds.onenet.iot.api.entity.application.device;

import cm.heclouds.onenet.iot.api.entity.application.BaseDeviceRequest;

/**
 * 设备详情请求
 * @author ChengQi
 * @date 2020-07-06 16:23
 */
public class QueryDeviceDetailRequest extends BaseDeviceRequest<QueryDeviceDetailResponse> {

    public QueryDeviceDetailRequest() {
        super(Method.GET, "detail");
    }

    /**
     * 设置产品ID
     * @param productId 产品ID
     */
    public void setProductId(String productId) {
        queryParam("product_id", productId);
    }

    /**
     * 设置设备名称
     * @param deviceName 设备名称
     */
    public void setDeviceName(String deviceName) {
        queryParam("device_name", deviceName);
    }

    @Override
    protected Class<QueryDeviceDetailResponse> getResponseType() {
        return QueryDeviceDetailResponse.class;
    }
}
