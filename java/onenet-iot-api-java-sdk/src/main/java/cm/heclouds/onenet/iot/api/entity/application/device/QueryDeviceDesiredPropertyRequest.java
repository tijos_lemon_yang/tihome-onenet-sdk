package cm.heclouds.onenet.iot.api.entity.application.device;

import cm.heclouds.onenet.iot.api.AbstractRequest;
import cm.heclouds.onenet.iot.api.entity.application.BaseThingModelRequest;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 设备属性期望查询请求
 */
public class QueryDeviceDesiredPropertyRequest extends BaseThingModelRequest<QueryDeviceDesiredPropertyResponse> {

    private List<String> params = new ArrayList<>();

    public QueryDeviceDesiredPropertyRequest() {
        super(AbstractRequest.Method.POST, "query-device-desired-property");
    }

    /**
     * 设置产品ID
     * @param productId 产品ID
     */
    public void setProductId(String productId) {
        bodyParam("product_id", productId);
    }

    /**
     * 设置设备名称
     * @param deviceName 设备名称
     */
    public void setDeviceName(String deviceName) {
        bodyParam("device_name", deviceName);
    }

    /**
     * 设置查询期望值的功能点标识集合，参数不传默认查询所有属性期望
     * @param params 查询期望值的功能点标识集合
     */
    public void setParams(List<String> params) {
        this.params = params;
        bodyParam("params", this.params);
    }

    /**
     * 添加要查询的期望值的功能点标识
     * @param param 属性功能点标识
     */
    public void addParam(String param) {
        params.add(param);
        bodyParam("params", params);
    }

    @Override
    protected Class<QueryDeviceDesiredPropertyResponse> getResponseType() {
        return QueryDeviceDesiredPropertyResponse.class;
    }

    @Override
    protected QueryDeviceDesiredPropertyResponse newResponse(String responseBody) {
        QueryDeviceDesiredPropertyResponse response = new QueryDeviceDesiredPropertyResponse();
        if (StringUtils.isEmpty(responseBody)) {
            return response;
        }

        Map<String, DesiredProperty> paramsMap = JSON.parseObject(responseBody, new TypeReference<Map<String, DesiredProperty>>(){});
        response.putAll(paramsMap);
        return response;
    }
}
