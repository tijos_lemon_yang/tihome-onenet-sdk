package cm.heclouds.onenet.iot.api.entity;

import cm.heclouds.onenet.iot.api.AbstractRequest;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

/**
 * Pageable request of OneNET Studio API,
 * it providers {@code #setOffset} and {@code #setLimit} methods
 * @author ChengQi
 * @date 2020-07-06 14:27
 */
public abstract class AbstractPageableRequest<T extends AbstractPagedResponse<?>> extends AbstractRequest<T> {

    public AbstractPageableRequest(String nameSpace, String action) {
        super(nameSpace, Method.GET, action);
    }

    /**
     * 设置请求起始位置
     * @param offset 请求起始位置，默认0
     */
    public void setOffset(Integer offset) {
        queryParam("offset", offset);
    }

    /**
     * 设置每次请求记录数
     * @param limit 每次请求记录数
     */
    public void setLimit(Integer limit) {
        queryParam("limit", limit);
    }

    @Override
    protected T newResponse(String responseBody) {
        //T response = JSON.parseObject(responseBody, getResponseType());

       T response =  JSONArray.parseObject(responseBody, getResponseType());

        JSONObject responseObject = JSON.parseObject(responseBody);

        if(responseObject.containsKey("meta")){
            JSONObject metaObject = responseObject.getJSONObject("meta");
        }
        else{
            Meta meta = new Meta();
            meta.setLimit(responseObject.getInteger("limit"));
            meta.setOffset(responseObject.getInteger("offset"));
            response.setMeta(meta);
        }

        return response;
    }
}
