package cm.heclouds.onenet.iot.api.entity.enums;

import cm.heclouds.onenet.iot.api.json.ValueHolder;

/**
 * 请求类型 枚举类型
 * @author ChengQi
 * @date 2020-07-10 16:54
 */
public enum ReadWriteType implements ValueHolder<Integer> {

    /**
     * 写
     */
    WRITE(0),
    /**
     * 读
     */
    READ(1);

    private final Integer value;

    ReadWriteType(Integer value) {
        this.value = value;
    }

    @Override
    public Integer getValue() {
        return value;
    }
}
