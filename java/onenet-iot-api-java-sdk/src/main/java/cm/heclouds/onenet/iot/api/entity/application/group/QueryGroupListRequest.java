package cm.heclouds.onenet.iot.api.entity.application.group;

import cm.heclouds.onenet.iot.api.entity.application.BasePageableGroupRequest;

/**
 * 分组列表请求
 */
public class QueryGroupListRequest extends BasePageableGroupRequest<QueryGroupListResponse> {

    public QueryGroupListRequest() {
        super("list");
    }

    /**
     * 设置分组名称
     * @param name 分组名称
     */
    public void setName(String name) {
        queryParam("name", name);
    }

    /**
     * 设置标签key
     * @param key 标签key（key、value需成对出现，否则没有效果）
     */
    public void setTagKey(String key) {
        queryParam("key", key);
    }

    /**
     * 设置标签value
     * @param value 标签value（key、value 需成对出现，否则没有效果）
     */
    public void setTagValue(String value) {
        queryParam("value", value);
    }

    @Override
    protected Class<QueryGroupListResponse> getResponseType() {
        return QueryGroupListResponse.class;
    }
}
