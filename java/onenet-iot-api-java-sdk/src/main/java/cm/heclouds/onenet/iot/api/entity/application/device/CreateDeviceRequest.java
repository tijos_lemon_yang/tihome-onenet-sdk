package cm.heclouds.onenet.iot.api.entity.application.device;

import cm.heclouds.onenet.iot.api.entity.application.BaseDeviceRequest;

/**
 * 设备创建请求
 */
public class CreateDeviceRequest extends BaseDeviceRequest<CreateDeviceResponse> {

    public CreateDeviceRequest() {
        super(Method.POST, "create");
    }

    /**
     * 设置产品ID
     * @param productId 产品ID
     */
    public void setProductId(String productId) {
        bodyParam("product_id", productId);
    }

    /**
     * 设置设备名称参数
     * @param deviceName 设备名称
     */
    public void setDeviceName(String deviceName) {
        bodyParam("device_name", deviceName);
    }

    /**
     * 设置设备描述参数
     * @param desc 设备描述
     */
    public void setDesc(String desc) {
        bodyParam("desc", desc);
    }

    @Override
    protected Class<CreateDeviceResponse> getResponseType() {
        return CreateDeviceResponse.class;
    }
}
