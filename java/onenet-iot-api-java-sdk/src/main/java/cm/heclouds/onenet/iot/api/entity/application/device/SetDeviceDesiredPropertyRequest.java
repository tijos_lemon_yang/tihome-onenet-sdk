package cm.heclouds.onenet.iot.api.entity.application.device;

import cm.heclouds.onenet.iot.api.entity.application.BaseDevicePropertyRequest;

/**
 * 设备属性期望设置请求
 */
public class SetDeviceDesiredPropertyRequest extends BaseDevicePropertyRequest<SetDeviceDesiredPropertyResponse> {

    public SetDeviceDesiredPropertyRequest() {
        super(Method.POST, "set-device-desired-property");
    }

    @Override
    protected Class<SetDeviceDesiredPropertyResponse> getResponseType() {
        return SetDeviceDesiredPropertyResponse.class;
    }

    @Override
    protected SetDeviceDesiredPropertyResponse newResponse(String responseBody) {
        return new SetDeviceDesiredPropertyResponse();
    }
}
