package cm.heclouds.onenet.iot.api.entity.application.group;

import cm.heclouds.onenet.iot.api.entity.application.BaseGroupRequest;

import java.util.HashMap;
import java.util.Map;

/**
 * 分组编辑请求
 */
public class UpdateGroupRequest extends BaseGroupRequest<UpdateGroupResponse> {

    private Map<String, String> tags = new HashMap<>();

    public UpdateGroupRequest() {
        super(Method.POST, "update");
    }

    /**
     * 设置分组ID
     * @param id 分组ID
     */
    public void setId(String id) {
        bodyParam("group_id", id);
    }

    /**
     * 设置标签信息
     * @param tags 标签信息
     */
    public void setTags(Map<String, String> tags) {
        this.tags = tags;
        bodyParam("tag", this.tags);
    }

    /**
     * 添加标签信息
     * @param key 键
     * @param value 值
     */
    public void addTag(String key, String value) {
        tags.put(key, value);
        bodyParam("tag", this.tags);
    }

    /**
     * 设置分组描述
     * @param desc 分组描述
     */
    public void setDesc(String desc) {
        bodyParam("desc", desc);
    }

    @Override
    protected Class<UpdateGroupResponse> getResponseType() {
        return UpdateGroupResponse.class;
    }
}
