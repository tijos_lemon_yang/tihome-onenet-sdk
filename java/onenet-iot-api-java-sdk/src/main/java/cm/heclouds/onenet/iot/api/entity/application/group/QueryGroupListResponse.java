package cm.heclouds.onenet.iot.api.entity.application.group;

import cm.heclouds.onenet.iot.api.entity.AbstractPagedResponse;

/**
 * 分组列表响应
 */
public class QueryGroupListResponse extends AbstractPagedResponse<GroupInfo> {

}
