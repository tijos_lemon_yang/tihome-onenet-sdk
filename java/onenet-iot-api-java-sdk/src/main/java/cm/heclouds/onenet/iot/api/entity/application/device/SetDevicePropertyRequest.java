package cm.heclouds.onenet.iot.api.entity.application.device;

import cm.heclouds.onenet.iot.api.entity.application.BaseDevicePropertyRequest;

/**
 * 设备属性设置请求
 * <a href="https://open.iot.10086.cn/doc/iot_platform/book/api/application/setDeviceProperty.html">设备属性设置</a>
 * @author ChengQi
 * @date 2020-07-06 17:20
 */
public class SetDevicePropertyRequest extends BaseDevicePropertyRequest<SetDevicePropertyResponse> {

    public SetDevicePropertyRequest() {
        super(Method.POST, "set-device-property");
    }

    @Override
    protected Class<SetDevicePropertyResponse> getResponseType() {
        return SetDevicePropertyResponse.class;
    }
}
