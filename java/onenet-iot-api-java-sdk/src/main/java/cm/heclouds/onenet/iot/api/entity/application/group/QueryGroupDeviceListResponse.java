package cm.heclouds.onenet.iot.api.entity.application.group;

import cm.heclouds.onenet.iot.api.entity.AbstractPagedResponse;
import cm.heclouds.onenet.iot.api.entity.common.Device;

/**
 * 分组列表响应
 */
public class QueryGroupDeviceListResponse extends AbstractPagedResponse<Device> {


}
