package cm.heclouds.onenet.iot.api.entity.application.device;

import cm.heclouds.onenet.iot.api.IotResponse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

/**
 * 设备属性数据查询
 */
public class QueryDevicePropertyDetailResponse extends HashMap<String, Object> implements IotResponse {

    private String requestId;

    @Override
    public String getRequestId() {
        return requestId;
    }

    @Override
    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }
}
