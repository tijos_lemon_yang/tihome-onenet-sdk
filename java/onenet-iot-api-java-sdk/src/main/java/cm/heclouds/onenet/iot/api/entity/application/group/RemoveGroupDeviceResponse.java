package cm.heclouds.onenet.iot.api.entity.application.group;

import cm.heclouds.onenet.iot.api.IotResponse;
import cm.heclouds.onenet.iot.api.entity.common.ErrorData;

import java.util.ArrayList;

/**
 * 分组设备移除响应
 */
public class RemoveGroupDeviceResponse extends ArrayList<ErrorData> implements IotResponse {

    private String requestId;

    @Override
    public String getRequestId() {
        return requestId;
    }

    @Override
    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }
}
