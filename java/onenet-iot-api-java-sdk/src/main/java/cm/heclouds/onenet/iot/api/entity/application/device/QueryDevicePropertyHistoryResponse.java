package cm.heclouds.onenet.iot.api.entity.application.device;

import cm.heclouds.onenet.iot.api.entity.AbstractPagedResponse;

/**
 * 设备属性历史数据查询响应
 * <a href="https://open.iot.10086.cn/doc/iot_platform/book/api/application/queryDevicePropertyHistory.html">
 *     设备属性历史数据查询</a>
 * @author ChengQi
 * @date 2020/7/7
 */
public class QueryDevicePropertyHistoryResponse extends AbstractPagedResponse<DeviceHistoryProperty> {

}
