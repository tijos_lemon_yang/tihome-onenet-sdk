package cm.heclouds.onenet.iot.api.enums;

import cm.heclouds.onenet.iot.api.json.ValueHolder;
import cm.heclouds.onenet.iot.api.json.ValueHolderDeserializer;
import cm.heclouds.onenet.iot.api.json.ValueHolderSerializer;
import com.alibaba.fastjson.annotation.JSONType;

/**
 * @author ChengQi
 * @date 2020/7/7
 */
@JSONType(
        serializer = ValueHolderSerializer.class,
        deserializer = ValueHolderDeserializer.class,
        serializeEnumAsJavaBean = true)
public enum Sort implements ValueHolder<Integer> {

    /**
     * positive sequence
     */
    ASC(1),
    /**
     * reverse order
     */
    DESC(2);

    private final int value;

    Sort(int value) {
        this.value = value;
    }

    @Override
    public Integer getValue() {
        return value;
    }
}
