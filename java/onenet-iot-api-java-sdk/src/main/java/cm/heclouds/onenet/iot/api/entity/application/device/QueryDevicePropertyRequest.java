package cm.heclouds.onenet.iot.api.entity.application.device;

import cm.heclouds.onenet.iot.api.entity.application.BaseThingModelRequest;
import com.alibaba.fastjson.JSON;
import org.apache.commons.lang3.StringUtils;

import java.util.List;

/**
 * 设备属性最新数据查询请求
 */
public class QueryDevicePropertyRequest extends BaseThingModelRequest<QueryDevicePropertyResponse> {

    public QueryDevicePropertyRequest() {
        super(Method.GET, "query-device-property");
    }

    /**
     * 设置产品ID
     * @param productId 产品ID
     */
    public void setProductId(String productId) {
        queryParam("product_id", productId);
    }

    /**
     * 设置设备名称
     * @param deviceName 设备名称
     */
    public void setDeviceName(String deviceName) {
        queryParam("device_name", deviceName);
    }

    @Override
    protected Class<QueryDevicePropertyResponse> getResponseType() {
        return QueryDevicePropertyResponse.class;
    }

    @Override
    protected QueryDevicePropertyResponse newResponse(String responseBody) {
        QueryDevicePropertyResponse response = new QueryDevicePropertyResponse();
        if (StringUtils.isEmpty(responseBody)) {
            return response;
        }

        List<DeviceProperty> deviceProperties = JSON.parseArray(responseBody, DeviceProperty.class);
        response.addAll(deviceProperties);
        return response;
    }
}
