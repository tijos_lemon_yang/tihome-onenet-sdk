package cm.heclouds.onenet.iot.api.entity.application.device;

import cm.heclouds.onenet.iot.api.AbstractResponse;
import com.alibaba.fastjson.annotation.JSONField;
import cm.heclouds.onenet.iot.api.constant.Constant;
import cm.heclouds.onenet.iot.api.entity.enums.Status;
import cm.heclouds.onenet.iot.api.json.ValueHolderDeserializer;

import java.util.Date;

/**
 * 设备详情响应
 */
public class QueryDeviceDetailResponse extends AbstractResponse {

    /**
     * 设备名称
     */
    @JSONField(name = "name")
    private String deviceName;

    /**
     * 产品ID
     */
    @JSONField(name = "pid")
    private String productId;

    /**
     * 设备描述
     */
    private String desc;

    /**
     * 设备状态
     */
    @JSONField(deserializeUsing = ValueHolderDeserializer.class)
    private Status status;

    /**
     * 创建时间
     */
    @JSONField(name = "created_time")
    private String createdTime;

    /**
     * 最后一次在线时间
     */
    @JSONField(name = "last_time")
    private String lastTime;

    /**
     * 激活时间
     */
    @JSONField(name = "active_time")
    private String activeTime;

    /**
     * 设备密钥
     */
    @JSONField(name = "sec_key")
    private String secKey;

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(String createdTime) {
        this.createdTime = createdTime;
    }

    public String getLastTime() {
        return lastTime;
    }

    public void setLastTime(String lastTime) {
        this.lastTime = lastTime;
    }

    public String getActiveTime() {
        return activeTime;
    }

    public void setActiveTime(String activeTime) {
        this.activeTime = activeTime;
    }

    public String getSecKey() {
        return secKey;
    }

    public void setSecKey(String secKey) {
        this.secKey = secKey;
    }
}
