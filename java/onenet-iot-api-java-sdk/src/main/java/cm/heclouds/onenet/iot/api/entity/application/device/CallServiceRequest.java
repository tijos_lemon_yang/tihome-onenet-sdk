package cm.heclouds.onenet.iot.api.entity.application.device;

import cm.heclouds.onenet.iot.api.entity.application.BaseThingModelRequest;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * 调用服务请求
 */
public class CallServiceRequest extends BaseThingModelRequest<CallServiceResponse> {

    private Map<String, Object> params = new HashMap<>();

    public CallServiceRequest() {
        super(Method.POST, "call-service");
    }

    /**
     * 设置产品ID
     * @param productId 产品ID
     */
    public void setProductId(String productId) {
        bodyParam("product_id", productId);
    }

    /**
     * 设置设备唯一标识
     * @param deviceName 设备唯一标识
     */
    public void setDeviceName(String deviceName) {
        bodyParam("device_name", deviceName);
    }

    /**
     * 设置服务型功能点标识
     * @param identifier 服务型功能点标识
     */
    public void setIdentifier(String identifier) {
        bodyParam("identifier", identifier);
    }

    /**
     * 设置输入参数
     * @param params 输入参数的键值对
     */
    public void setParams(Map<String, Object> params) {
        this.params = params;
        bodyParam("params", this.params);
    }

    /**
     * 添加输入参数的键值对
     * @param key 输入参数的唯一标识
     * @param value 输入参数的值
     */
    public void addParam(String key, Object value) {
        params.put(key, value);
        bodyParam("params", this.params);
    }

    @Override
    protected Class<CallServiceResponse> getResponseType() {
        return CallServiceResponse.class;
    }

    @Override
    protected CallServiceResponse newResponse(String responseBody) {
        CallServiceResponse response = new CallServiceResponse();
        if (StringUtils.isEmpty(responseBody)) {
            return response;
        }
        JSONObject jsonObject = JSON.parseObject(responseBody);
        response.putAll(jsonObject);
        return response;
    }
}
