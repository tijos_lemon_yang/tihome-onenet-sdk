package cm.heclouds.onenet.iot.api.entity.application;

import cm.heclouds.onenet.iot.api.AbstractRequest;
import cm.heclouds.onenet.iot.api.IotResponse;
import cm.heclouds.onenet.iot.api.constant.Constant;

/**
 * Application developing request of OneNET Studio API
 * @author ChengQi
 * @date 2020-07-02 14:05
 */
public abstract class BaseGroupRequest<T extends IotResponse> extends AbstractRequest<T> {

    public BaseGroupRequest(Method method, String action) {
        super(Constant.OPEN_API_NAMESPACE_GROUP, method, action);
    }
}
