package cm.heclouds.onenet.iot.api.entity.application.device;

import cm.heclouds.onenet.iot.api.entity.application.BaseThingModelRequest;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.List;

/**
 * 设备属性最新数据查询请求
 */
public class QueryDevicePropertyDetailRequest extends BaseThingModelRequest<QueryDevicePropertyDetailResponse> {

    public QueryDevicePropertyDetailRequest() {
        super(Method.POST, "query-device-property-detail");
    }

    /**
     * 设置产品ID
     * @param productId 产品ID
     */
    public void setProductId(String productId) {
        bodyParam("product_id", productId);
    }

    /**
     * 设置设备名称
     * @param deviceName 设备名称
     */
    public void setDeviceName(String deviceName) {
        bodyParam("device_name", deviceName);
    }

    public void setPropertyIds(String [] propertyIds) { bodyParam( "params", propertyIds);}
    @Override
    protected Class<QueryDevicePropertyDetailResponse> getResponseType() {
        return QueryDevicePropertyDetailResponse.class;
    }

    @Override
    protected QueryDevicePropertyDetailResponse newResponse(String responseBody) {
        QueryDevicePropertyDetailResponse response = new QueryDevicePropertyDetailResponse();
        if (StringUtils.isEmpty(responseBody)) {
            return response;
        }

        response = JSON.parseObject(responseBody, QueryDevicePropertyDetailResponse.class);
        return response;
    }
}
