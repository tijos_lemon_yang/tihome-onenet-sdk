package cm.heclouds.onenet.iot.api.entity.application.device;

import cm.heclouds.onenet.iot.api.entity.AbstractPagedResponse;

/**
 * 设备操作日志查询响应
 */
public class QueryDeviceLogResponse extends AbstractPagedResponse<DeviceLog> {
}
