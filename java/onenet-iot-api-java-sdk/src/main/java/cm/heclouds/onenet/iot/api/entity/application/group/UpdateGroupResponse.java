package cm.heclouds.onenet.iot.api.entity.application.group;

import cm.heclouds.onenet.iot.api.AbstractResponse;

/**
 * 分组编辑响应
 */
public class UpdateGroupResponse extends AbstractResponse {

    /**
     * 分组ID
     */
    private String groupId;

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }
}
