package cm.heclouds.onenet.iot.api.entity.application.device;

import com.alibaba.fastjson.annotation.JSONField;
import cm.heclouds.onenet.iot.api.entity.enums.ReadWriteType;
import cm.heclouds.onenet.iot.api.json.ValueHolderDeserializer;

import java.util.Date;

/**
 * 设备操作日志
 * @author ChengQi
 * @date 2020/7/7
 */
public class DeviceLog {

    /**
     * 请求类型
     */
    @JSONField(deserializeUsing = ValueHolderDeserializer.class)
    private ReadWriteType type;

    /**
     * 请求时间
     */
    @JSONField(name = "request_time")
    private Date requestTime;

    /**
     * 请求内容 k=>v形式， k为属性功能点标识，v为功能点设置值
     */
    @JSONField(name = "request_body")
    private String requestBody;

    /**
     * 响应时间
     */
    @JSONField(name = "response_time")
    private Date responseTime;

    /**
     * 响应结果, 设备回复响应中的msg字段
     */
    @JSONField(name = "response_body")
    private String responseBody;

    @JSONField(name = "code")
    private  int code;

    @JSONField(name = "msg")
    private  String msg;

    public ReadWriteType getType() {
        return type;
    }

    public void setType(ReadWriteType type) {
        this.type = type;
    }

    public Date getRequestTime() {
        return requestTime;
    }

    public void setRequestTime(Date requestTime) {
        this.requestTime = requestTime;
    }

    public String getRequestBody() {
        return requestBody;
    }

    public void setRequestBody(String requestBody) {
        this.requestBody = requestBody;
    }

    public Date getResponseTime() {
        return responseTime;
    }

    public void setResponseTime(Date responseTime) {
        this.responseTime = responseTime;
    }

    public String getResponseBody() {
        return responseBody;
    }

    public void setResponseBody(String responseBody) {
        this.responseBody = responseBody;
    }

    public String getMsg(){ return msg;};

    public void setMsg(String msg) {this.msg = msg;}

    public int getCode(){return code;}

    public void setCode(int code){this.code = code;}
}
