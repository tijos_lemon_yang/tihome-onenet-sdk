package cm.heclouds.onenet.iot.api.entity.application.group;

import cm.heclouds.onenet.iot.api.entity.application.BaseGroupRequest;

/**
 * 分组详情请求
 */
public class QueryGroupDetailRequest extends BaseGroupRequest<QueryGroupDetailResponse> {

    public QueryGroupDetailRequest() {
        super(Method.GET, "detail");
    }

    /**
     * 设置分组ID
     * @param id 分组ID
     */
    public void setId(String id) {
        queryParam("group_id", id);
    }

    @Override
    protected Class<QueryGroupDetailResponse> getResponseType() {
        return QueryGroupDetailResponse.class;
    }
}
