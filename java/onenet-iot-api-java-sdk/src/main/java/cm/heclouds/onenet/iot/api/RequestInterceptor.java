package cm.heclouds.onenet.iot.api;

import cm.heclouds.onenet.iot.api.json.ValueHolder;
import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

import java.io.IOException;
import java.util.Map;
import java.util.Objects;

/**
 * The implement class of {@link Interceptor} which helping to build request
 * @author ChengQi
 * @date 2020-07-03 9:19
 */
public class RequestInterceptor implements Interceptor {

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        AbstractRequest<?> tag = request.tag(AbstractRequest.class);
        HttpUrl.Builder urlBuilder = request.url().newBuilder();
        urlBuilder.addPathSegment(Objects.requireNonNull(tag).getNameSpace());
        urlBuilder.addPathSegment(Objects.requireNonNull(tag).getAction());

        Map<String, Object> queryParams = tag.getQueryParams();
        queryParams.forEach((name, value) -> {
            String parameter = null;
            if (!Objects.isNull(value)) {
                parameter = value.toString();
                if (value instanceof ValueHolder) {
                    parameter = ((ValueHolder<?>) value).getValue().toString();
                }
            }
            urlBuilder.addQueryParameter(name, parameter);
        });
        request = request.newBuilder().url(urlBuilder.build()).build();
        return chain.proceed(request);
    }
}
