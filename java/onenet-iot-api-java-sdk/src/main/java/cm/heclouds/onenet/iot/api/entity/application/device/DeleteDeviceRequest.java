package cm.heclouds.onenet.iot.api.entity.application.device;

import cm.heclouds.onenet.iot.api.AbstractRequest;
import cm.heclouds.onenet.iot.api.entity.application.BaseDeviceRequest;

/**
 * 设备删除请求
 */
public class DeleteDeviceRequest extends BaseDeviceRequest<DeleteDeviceResponse> {

    public DeleteDeviceRequest() {
        super(AbstractRequest.Method.POST, "delete");
    }

    /**
     * 设置产品ID
     * @param productId 产品ID
     */
    public void setProductId(String productId) {
        bodyParam("product_id", productId);
    }


    /**
     * 设置设备名称参数
     * @param deviceName 设备名称
     */
    public void setDeviceName(String deviceName) {
        bodyParam("device_name", deviceName);
    }

    @Override
    protected Class<DeleteDeviceResponse> getResponseType() {
        return DeleteDeviceResponse.class;
    }

    @Override
    protected DeleteDeviceResponse newResponse(String responseBody) {
        return new DeleteDeviceResponse();
    }
}
