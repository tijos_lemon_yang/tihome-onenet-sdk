package cm.heclouds.onenet.iot.api.entity.application.group;

import cm.heclouds.onenet.iot.api.entity.application.BaseGroupRequest;

/**
 * 分组删除请求
 */
public class DeleteGroupRequest extends BaseGroupRequest<DeleteGroupResponse> {

    public DeleteGroupRequest() {
        super(Method.POST, "delete");
    }

    /**
     * 设置项目ID
     * @param projectId 项目ID
     */
    public void setProjectId(String projectId) {
        bodyParam("project_id", projectId);
    }

    /**
     * 设置分组ID
     * @param id 分组ID
     */
    public void setId(String id) {
        bodyParam("group_id", id);
    }

    @Override
    protected Class<DeleteGroupResponse> getResponseType() {
        return DeleteGroupResponse.class;
    }

    @Override
    protected DeleteGroupResponse newResponse(String responseBody) {
        return new DeleteGroupResponse();
    }
}
