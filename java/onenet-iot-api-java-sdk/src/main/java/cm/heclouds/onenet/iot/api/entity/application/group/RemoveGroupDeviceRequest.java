package cm.heclouds.onenet.iot.api.entity.application.group;

import cm.heclouds.onenet.iot.api.entity.application.BaseGroupRequest;
import cm.heclouds.onenet.iot.api.entity.common.ErrorData;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.StringUtils;

import java.util.List;

/**
 * 分组设备移除请求
 */
public class RemoveGroupDeviceRequest extends BaseGroupRequest<RemoveGroupDeviceResponse> {

    public RemoveGroupDeviceRequest() {
        super(Method.POST, "del-devices");
    }

    /**
     * 设置项目ID
     * @param projectId 项目ID
     */
    public void setProjectId(String projectId) {
        bodyParam("project_id", projectId);
    }

    /**
     * 设置分组ID
     * @param id 分组ID
     */
    public void setId(String id) {
        bodyParam("group_id", id);
    }

    /**
     * 设置产品ID
     * @param productId 产品ID
     */
    public void setProductId(String productId) {
        bodyParam("product_id", productId);
    }

    /**
     * 设置需要移除的设备集合
     * @param devices 需要移除的设备集合
     */
    public void setDevices(List<String> devices) {
        bodyParam("device_name_list", devices);
    }

    @Override
    protected Class<RemoveGroupDeviceResponse> getResponseType() {
        return RemoveGroupDeviceResponse.class;
    }

    @Override
    protected RemoveGroupDeviceResponse newResponse(String responseBody) {
        RemoveGroupDeviceResponse response = new RemoveGroupDeviceResponse();
        if (StringUtils.isEmpty(responseBody)) {
            return response;
        }
        JSONObject jsonObject = JSONObject.parseObject(responseBody);
        if (!jsonObject.containsKey("error_data")) {
            return response;
        }
        List<ErrorData> errorData = jsonObject.getJSONArray("error_data").toJavaList(ErrorData.class);
        response.addAll(errorData);
        return response;
    }
}
