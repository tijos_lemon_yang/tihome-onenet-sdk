package cm.heclouds.onenet.iot.api.entity.application.device;

import com.alibaba.fastjson.annotation.JSONField;
import cm.heclouds.onenet.iot.api.entity.enums.Status;
import cm.heclouds.onenet.iot.api.json.ValueHolderDeserializer;

import java.util.Date;

/**
 * 设备历史状态
 * @author ChengQi
 * @date 2020-07-06 16:50
 */
public class DeviceStatus {

    /**
     * 设备状态
     */
    @JSONField(deserializeUsing = ValueHolderDeserializer.class)
    private Status status;

    /**
     * 时间
     */
    private Date time;

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }
}
