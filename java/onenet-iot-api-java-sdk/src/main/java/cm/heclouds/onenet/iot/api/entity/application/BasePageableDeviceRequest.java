package cm.heclouds.onenet.iot.api.entity.application;

import cm.heclouds.onenet.iot.api.constant.Constant;
import cm.heclouds.onenet.iot.api.entity.AbstractPageableRequest;
import cm.heclouds.onenet.iot.api.entity.AbstractPagedResponse;

/**
 * Application developing request of OneNET Studio API which is pageable
 * @author ChengQi
 * @date 2020-07-06 14:37
 */
public abstract class BasePageableDeviceRequest<T extends AbstractPagedResponse<?>>
        extends AbstractPageableRequest<T> {

    public BasePageableDeviceRequest(String action) {
        super(Constant.OPEN_API_NAMESPACE_DEVICE, action);
    }
}
