package cm.heclouds.onenet.iot.api.entity.application.group;

import cm.heclouds.onenet.iot.api.entity.application.BasePageableGroupRequest;
import cm.heclouds.onenet.iot.api.entity.application.device.QueryDeviceDesiredPropertyResponse;
import cm.heclouds.onenet.iot.api.entity.common.Device;
import com.alibaba.fastjson.JSONArray;

import java.util.List;

/**
 * 分组列表请求
 */
public class QueryGroupDeviceListRequest extends BasePageableGroupRequest<QueryGroupDeviceListResponse> {

    public QueryGroupDeviceListRequest() {
        super("devices");
    }

    /**
     * 设置分组ID
     *
     * @param id 分组ID
     */
    public void setId(String id) {
        queryParam("group_id", id);
    }

    @Override
    protected Class<QueryGroupDeviceListResponse> getResponseType() {
        return QueryGroupDeviceListResponse.class;
    }

    @Override
    protected QueryGroupDeviceListResponse newResponse(String responseBody) {
        List<Device> deviceList = JSONArray.parseArray(responseBody, Device.class);

        QueryGroupDeviceListResponse response = new QueryGroupDeviceListResponse();
        response.setList(deviceList);
        return response;
    }
}