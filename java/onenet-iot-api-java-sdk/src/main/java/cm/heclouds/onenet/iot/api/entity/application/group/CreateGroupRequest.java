package cm.heclouds.onenet.iot.api.entity.application.group;

import cm.heclouds.onenet.iot.api.entity.application.BaseGroupRequest;

/**
 * 分组创建请求
 */
public class CreateGroupRequest extends BaseGroupRequest<CreateGroupResponse> {

    public CreateGroupRequest() {
        super(Method.POST, "create");
    }

    /**
     * 设置项目ID
     * @param projectId 项目ID
     */
    public void setProjectId(String projectId) {
        bodyParam("project_id", projectId);
    }

    /**
     * 设置分组名称
     * @param name 分组名称
     */
    public void setName(String name) {
        bodyParam("name", name);
    }

    /**
     * 设置分组描述
     * @param desc 分组描述
     */
    public void setDesc(String desc) {
        bodyParam("desc", desc);
    }

    @Override
    protected Class<CreateGroupResponse> getResponseType() {
        return CreateGroupResponse.class;
    }
}
