package cm.heclouds.onenet.iot.api.test;

import cm.heclouds.onenet.iot.api.entity.application.device.*;
import com.alibaba.fastjson.JSON;
import cm.heclouds.onenet.iot.api.exception.IotClientException;
import cm.heclouds.onenet.iot.api.exception.IotServerException;
import org.junit.Test;

import java.util.concurrent.CountDownLatch;

/**
 * 设备管理类-API调用单元测试
 * @author ChengQi
 * @date 2020-07-02 14:21
 */
@SuppressWarnings({"FieldCanBeLocal", "Duplicates"})
public class DeviceManagerApiTest extends ApiTest {

    private final String productId = "5POpr2bk2g";

    private final String deviceId = "861658064469516";

    /**
     * 同步调用设备创建API
     */
    @Test
    public void testCreateDevice() {
        CreateDeviceRequest request = new CreateDeviceRequest();
        request.setProductId(productId);
        request.setDeviceName(deviceId);
        request.setDesc("new device ");

        try {
            CreateDeviceResponse response = client.sendRequest(request);
            System.out.println(JSON.toJSONString(response));
        } catch (IotClientException e) {
            e.printStackTrace();
        } catch (IotServerException e) {
            System.err.println(e.getCode());
            e.printStackTrace();
        }
    }

    /**
     * 异步调用设备创建API
     */
    @Test
    public void testCreateDeviceAsync() throws InterruptedException {
        CountDownLatch latch = new CountDownLatch(1);
        CreateDeviceRequest request = new CreateDeviceRequest();
        request.setProductId(productId);
        request.setDeviceName(deviceId);
        request.setDesc("new device desc");

        client.sendRequestAsync(request).whenComplete((response, cause) -> {
            if (response != null) {
                System.out.println(JSON.toJSONString(response));
            } else {
                if (cause instanceof IotServerException) {
                    IotServerException serverError = (IotServerException) cause;
                    System.err.println(serverError.getCode());
                }
                cause.printStackTrace();
            }
            latch.countDown();
        });
        latch.await();
    }


    /**
     * 同步调用设备编辑API
     */
    @Test
    public void testUpdateDevice() {
        UpdateDeviceRequest request = new UpdateDeviceRequest();
        request.setProductId(productId);
        request.setDeviceName(deviceId);
        request.setDesc("update device 描述");

        try {
            UpdateDeviceResponse response = client.sendRequest(request);
            System.out.println(JSON.toJSONString(response));
        } catch (IotClientException e) {
            e.printStackTrace();
        } catch (IotServerException e) {
            System.err.println(e.getCode());
            e.printStackTrace();
        }
    }

    /**
     * 异步调用设备编辑API
     */
    @Test
    public void testUpdateDeviceAsync() throws InterruptedException {
        CountDownLatch latch = new CountDownLatch(1);
        UpdateDeviceRequest request = new UpdateDeviceRequest();
        request.setProductId(productId);
        request.setDeviceName(deviceId);
        request.setDesc("update device 描述");

        client.sendRequestAsync(request).whenComplete((response, cause) -> {
            if (response != null) {
                System.out.println(JSON.toJSONString(response));
            } else {
                if (cause instanceof IotServerException) {
                    IotServerException serverError = (IotServerException) cause;
                    System.err.println(serverError.getCode());
                }
                cause.printStackTrace();
            }
            latch.countDown();
        });
        latch.await();
    }

    /**
     * 同步调用设备删除API
     */
    @Test
    public void testDeleteDevice() {
        DeleteDeviceRequest request = new DeleteDeviceRequest();
        request.setProductId(productId);
        request.setDeviceName(deviceId);

        try {
            DeleteDeviceResponse response = client.sendRequest(request);
            System.out.println(JSON.toJSONString(response));
        } catch (IotClientException e) {
            e.printStackTrace();
        } catch (IotServerException e) {
            System.err.println(e.getCode());
            e.printStackTrace();
        }
    }

    /**
     * 异步调用设备删除API
     */
    @Test
    public void testDeleteDeviceAsync() throws InterruptedException {
        CountDownLatch latch = new CountDownLatch(1);
        DeleteDeviceRequest request = new DeleteDeviceRequest();
        request.setProductId(productId);
        request.setDeviceName(deviceId);

        client.sendRequestAsync(request).whenComplete((response, cause) -> {
            if (response != null) {
                System.out.println(JSON.toJSONString(response));
            } else {
                if (cause instanceof IotServerException) {
                    IotServerException serverError = (IotServerException) cause;
                    System.err.println(serverError.getCode());
                }
                cause.printStackTrace();
            }
            latch.countDown();
        });
        latch.await();
    }

    /**
     * 同步调用设备详情API
     */
    @Test
    public void testQueryDeviceDetail() {
        QueryDeviceDetailRequest request = new QueryDeviceDetailRequest();
        request.setProductId(productId);
        request.setDeviceName(deviceId);

        try {
            QueryDeviceDetailResponse response = client.sendRequest(request);
            System.out.println(JSON.toJSONString(response));
        } catch (IotClientException e) {
            e.printStackTrace();
        } catch (IotServerException e) {
            System.err.println(e.getCode());
            e.printStackTrace();
        }
    }

    /**
     * 异步调用设备详情API
     */
    @Test
    public void testQueryDeviceDetailAsync() throws InterruptedException {
        CountDownLatch latch = new CountDownLatch(1);
        QueryDeviceDetailRequest request = new QueryDeviceDetailRequest();
        request.setProductId(productId);
        request.setDeviceName(deviceId);

        client.sendRequestAsync(request).whenComplete((response, cause) -> {
            if (response != null) {
                System.out.println(JSON.toJSONString(response));
            } else {
                if (cause instanceof IotServerException) {
                    IotServerException serverError = (IotServerException) cause;
                    System.err.println(serverError.getCode());
                }
                cause.printStackTrace();
            }
            latch.countDown();
        });
        latch.await();
    }


    /**
     * 同步调用服务API
     */
    @Test
    public void testCallService() {
        CallServiceRequest request = new CallServiceRequest();
        request.setProductId(productId);
        request.setDeviceName("del11");
        request.setIdentifier("adfaf");
        request.addParam("a", 1);

        try {
            CallServiceResponse response = client.sendRequest(request);
            System.out.println(JSON.toJSONString(response));
        } catch (IotClientException e) {
            e.printStackTrace();
        } catch (IotServerException e) {
            System.err.println(e.getCode());
            e.printStackTrace();
        }
    }

    /**
     * 异步调用服务API
     */
    @Test
    public void testCallServiceAsync() throws InterruptedException {
        CountDownLatch latch = new CountDownLatch(1);
        CallServiceRequest request = new CallServiceRequest();
        request.setProductId(productId);
        request.setDeviceName("api-sdk-device-001");
        request.setIdentifier("service-api");
        request.addParam("input-param1", 1);

        client.sendRequestAsync(request).whenComplete((response, cause) -> {
            if (response != null) {
                System.out.println(JSON.toJSONString(response));
            } else {
                if (cause instanceof IotServerException) {
                    IotServerException serverError = (IotServerException) cause;
                    System.err.println(serverError.getCode());
                }
                cause.printStackTrace();
            }
            latch.countDown();
        });
        latch.await();
    }
}
