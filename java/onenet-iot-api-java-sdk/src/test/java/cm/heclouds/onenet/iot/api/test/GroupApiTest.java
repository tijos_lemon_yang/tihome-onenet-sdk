package cm.heclouds.onenet.iot.api.test;

import cm.heclouds.onenet.iot.api.entity.application.group.*;
import com.alibaba.fastjson.JSON;
import cm.heclouds.onenet.iot.api.exception.IotClientException;
import cm.heclouds.onenet.iot.api.exception.IotServerException;
import org.junit.Test;

import java.util.Collections;
import java.util.concurrent.CountDownLatch;

/**
 * <p>应用开发类-分组管理API调用单元测试</p>
 */
public class GroupApiTest extends ApiTest {

    private final String productId = "5POpr2bk2g";

    private final  String groupId = "85f518c0-5deb-400d-97b6-1d6aa0a2753a";
    /**
     * 同步调用分组创建API
     */
    @Test
    public void testCreateGroup() {
        CreateGroupRequest request = new CreateGroupRequest();
        request.setName("group1");
        request.setDesc("分组1");

        try {
            CreateGroupResponse response = client.sendRequest(request);
            System.out.println(JSON.toJSONString(response));
        } catch (IotClientException e) {
            e.printStackTrace();
        } catch (IotServerException e) {
            System.err.println(e.getCode());
            e.printStackTrace();
        }
    }

    /**
     * 异步调用分组创建API
     */
    @Test
    public void testCreateGroupAsync() throws InterruptedException {
        CountDownLatch latch = new CountDownLatch(1);
        CreateGroupRequest request = new CreateGroupRequest();
        request.setName("group3");

        client.sendRequestAsync(request).whenComplete((response, cause) -> {
            if (response != null) {
                System.out.println(JSON.toJSONString(response));
            } else {
                if (cause instanceof IotServerException) {
                    IotServerException serverError = (IotServerException) cause;
                    System.err.println(serverError.getCode());
                }
                cause.printStackTrace();
            }
            latch.countDown();
        });
        latch.await();
    }

    /**
     * 同步调用分组删除API
     */
    @Test
    public void testDeleteGroup() {
        DeleteGroupRequest request = new DeleteGroupRequest();
        request.setId(groupId);

        try {
            DeleteGroupResponse response = client.sendRequest(request);
            System.out.println(response.getRequestId());
        } catch (IotClientException e) {
            e.printStackTrace();
        } catch (IotServerException e) {
            System.err.println(e.getCode());
            e.printStackTrace();
        }
    }

    /**
     * 异步调用分组删除API
     */
    @Test
    public void testDeleteGroupAsync() throws InterruptedException {
        CountDownLatch latch = new CountDownLatch(1);
        DeleteGroupRequest request = new DeleteGroupRequest();
        request.setId(groupId);

        client.sendRequestAsync(request).whenComplete((response, cause) -> {
            if (response != null) {
                System.out.println(response.getRequestId());
            } else {
                if (cause instanceof IotServerException) {
                    IotServerException serverError = (IotServerException) cause;
                    System.err.println(serverError.getCode());
                }
                cause.printStackTrace();
            }
            latch.countDown();
        });
        latch.await();
    }

    /**
     * 同步调用分组编辑API
     */
    @Test
    public void testUpdateGroup() {
        UpdateGroupRequest request = new UpdateGroupRequest();
        request.setId(groupId);
        request.addTag("haha", "xixi");
        request.setDesc("分组2");

        try {
            UpdateGroupResponse response = client.sendRequest(request);
            System.out.println(JSON.toJSONString(response));
        } catch (IotClientException e) {
            e.printStackTrace();
        } catch (IotServerException e) {
            System.err.println(e.getCode());
            e.printStackTrace();
        }
    }

    /**
     * 异步调用分组编辑API
     */
    @Test
    public void testUpdateGroupAsync() throws InterruptedException {
        CountDownLatch latch = new CountDownLatch(1);
        UpdateGroupRequest request = new UpdateGroupRequest();
        request.setId(groupId);
        request.addTag("kuku", "yiku");
        request.setDesc("分&&组2");

        client.sendRequestAsync(request).whenComplete((response, cause) -> {
            if (response != null) {
                System.out.println(JSON.toJSONString(response));
            } else {
                if (cause instanceof IotServerException) {
                    IotServerException serverError = (IotServerException) cause;
                    System.err.println(serverError.getCode());
                }
                cause.printStackTrace();
            }
            latch.countDown();
        });
        latch.await();
    }

    /**
     * 同步调用分组详情API
     */
    @Test
    public void testQueryGroupDetail() {
        QueryGroupDetailRequest request = new QueryGroupDetailRequest();
        request.setId(groupId);

        try {
            QueryGroupDetailResponse response = client.sendRequest(request);
            System.out.println(JSON.toJSONString(response));
        } catch (IotClientException e) {
            e.printStackTrace();
        } catch (IotServerException e) {
            System.err.println(e.getCode());
            e.printStackTrace();
        }
    }

    /**
     * 异步调用分组详情API
     */
    @Test
    public void testQueryGroupDetailAsync() throws InterruptedException {
        CountDownLatch latch = new CountDownLatch(1);
        QueryGroupDetailRequest request = new QueryGroupDetailRequest();
        request.setId(groupId);

        client.sendRequestAsync(request).whenComplete((response, cause) -> {
            if (response != null) {
                System.out.println(JSON.toJSONString(response));
            } else {
                if (cause instanceof IotServerException) {
                    IotServerException serverError = (IotServerException) cause;
                    System.err.println(serverError.getCode());
                }
                cause.printStackTrace();
            }
            latch.countDown();
        });
        latch.await();
    }

    /**
     * 同步调用分组列表API
     */
    @Test
    public void testQueryGroupList() {
        QueryGroupListRequest request = new QueryGroupListRequest();
        request.setTagKey("kuku");
        request.setTagValue("yiku");
        request.setOffset(0);
        request.setLimit(10);

        try {
            QueryGroupListResponse response = client.sendRequest(request);
            System.out.println(JSON.toJSONString(response));
        } catch (IotClientException e) {
            e.printStackTrace();
        } catch (IotServerException e) {
            System.err.println(e.getCode());
            e.printStackTrace();
        }
    }

    /**
     * 异步调用分组列表API
     */
    @Test
    public void testQueryGroupListAsync() throws InterruptedException {
        CountDownLatch latch = new CountDownLatch(1);
        QueryGroupListRequest request = new QueryGroupListRequest();
        request.setTagKey("kuku");
        request.setTagValue("yiku");
        request.setOffset(0);
        request.setLimit(10);

        client.sendRequestAsync(request).whenComplete((response, cause) -> {
           if (response != null) {
               System.out.println(JSON.toJSONString(response));
           } else {
               if (cause instanceof IotServerException) {
                   IotServerException serverError = (IotServerException) cause;
                   System.err.println(serverError.getCode());
               }
               cause.printStackTrace();
           }
           latch.countDown();
        });
        latch.await();
    }

    /**
     * 同步调用分组设备添加API
     */
    @Test
    public void testAddGroupDevice() {
        AddGroupDeviceRequest request = new AddGroupDeviceRequest();
        request.setId(groupId);
        request.setProductId(productId);
        request.setDevices(Collections.singletonList("861658064469516"));

        try {
            AddGroupDeviceResponse response = client.sendRequest(request);
            System.out.println(response.getRequestId());
            response.forEach(errorData -> System.out.println(JSON.toJSONString(errorData)));
        } catch (IotClientException e) {
            e.printStackTrace();
        } catch (IotServerException e) {
            System.err.println(e.getCode());
            e.printStackTrace();
        }
    }

    /**
     * 异步调用分组设备添加API
     */
    @Test
    public void testAddGroupDeviceAsync() throws InterruptedException {
        CountDownLatch latch = new CountDownLatch(1);
        AddGroupDeviceRequest request = new AddGroupDeviceRequest();
        request.setId(groupId);
        request.setProductId(productId);
        request.setDevices(Collections.singletonList("861658064469516"));

        client.sendRequestAsync(request).whenComplete((response, cause) -> {
            if (response != null) {
                System.out.println(response.getRequestId());
                response.forEach(errorData -> System.out.println(JSON.toJSONString(errorData)));
            } else {
                if (cause instanceof IotServerException) {
                    IotServerException serverError = (IotServerException) cause;
                    System.err.println(serverError.getCode());
                }
                cause.printStackTrace();
            }
            latch.countDown();
        });
        latch.await();
    }

    /**
     * 同步调用分组设备移除API
     */
    @Test
    public void testRemoveGroupDevice() {
        RemoveGroupDeviceRequest request = new RemoveGroupDeviceRequest();
        request.setId(groupId);
        request.setProductId(productId);
        request.setDevices(Collections.singletonList("861658064469516"));

        try {
            RemoveGroupDeviceResponse response = client.sendRequest(request);
            System.out.println(response.getRequestId());
            response.forEach(errorData -> System.out.println(JSON.toJSONString(errorData)));
        } catch (IotClientException e) {
            e.printStackTrace();
        } catch (IotServerException e) {
            System.err.println(e.getCode());
            e.printStackTrace();
        }
    }

    /**
     * 异步调用分组设备移除API
     */
    @Test
    public void testRemoveGroupDeviceAsync() throws InterruptedException {
        CountDownLatch latch = new CountDownLatch(1);
        RemoveGroupDeviceRequest request = new RemoveGroupDeviceRequest();
        request.setId(groupId);
        request.setProductId(productId);
        request.setDevices(Collections.singletonList("861658064469516"));

        client.sendRequestAsync(request).whenComplete((response, cause) -> {
            if (response != null) {
                System.out.println(response.getRequestId());
                response.forEach(errorData -> System.out.println(JSON.toJSONString(errorData)));
            } else {
                if (cause instanceof IotServerException) {
                    IotServerException serverError = (IotServerException) cause;
                    System.err.println(serverError.getCode());
                }
                cause.printStackTrace();
            }
            latch.countDown();
        });
        latch.await();
    }

}
