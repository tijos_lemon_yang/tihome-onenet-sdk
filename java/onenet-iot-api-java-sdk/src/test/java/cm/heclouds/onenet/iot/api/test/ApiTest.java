package cm.heclouds.onenet.iot.api.test;

import cm.heclouds.onenet.iot.api.IotClient;
import cm.heclouds.onenet.iot.api.IotProfile;
import cm.heclouds.onenet.iot.api.auth.SignatureMethod;
import org.junit.After;
import org.junit.Before;

import java.io.IOException;

/**
 * @author ChengQi
 * @date 2020-07-06 11:38
 */
@SuppressWarnings("FieldCanBeLocal")
public class ApiTest {

    private final String userId = "your user id";
    private final String accessKey = "your access key";
    IotClient client;

    /**
     * 初始化并构造{@link IotClient}
     */
    @Before
    public void initClient() {
        IotProfile profile = new IotProfile();
        profile
                .userId(userId)
                .accessKey(accessKey)
                .signatureMethod(SignatureMethod.SHA256);
        client = IotClient.create(profile);
    }

    /**
     * 关闭{@link IotClient}并释放资源
     * @throws IOException 如果有IO 错误
     */
    @After
    public void destroyClient() throws IOException {
        client.close();
    }
}
