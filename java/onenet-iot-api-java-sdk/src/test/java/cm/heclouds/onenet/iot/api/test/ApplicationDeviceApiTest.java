package cm.heclouds.onenet.iot.api.test;

import cm.heclouds.onenet.iot.api.entity.application.device.*;
import com.alibaba.fastjson.JSON;
import cm.heclouds.onenet.iot.api.entity.enums.EventType;
import cm.heclouds.onenet.iot.api.enums.Sort;
import cm.heclouds.onenet.iot.api.exception.IotClientException;
import cm.heclouds.onenet.iot.api.exception.IotServerException;
import org.junit.Test;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Date;
import java.util.concurrent.CountDownLatch;

/**
 * 应用开发类-设备操作API调用单元测试
 * @author ChengQi
 * @date 2020-07-06 16:27
 */
@SuppressWarnings("Duplicates")
public class ApplicationDeviceApiTest extends ApiTest {

    private final String productId = "5POpr2bk2g";
    private final String deviceId = "861658064469516";

    private final  String propertyId = "asr";
    /**
     * 同步调用设备详情API
     */
    @Test
    public void testQueryDeviceDetail() {
        QueryDeviceDetailRequest request = new QueryDeviceDetailRequest();
        request.setProductId("5POpr2bk2g");
        request.setDeviceName("861658064469516");

        try {
            QueryDeviceDetailResponse response = client.sendRequest(request);
            System.out.println(JSON.toJSONString(response));
        } catch (IotClientException e) {
            e.printStackTrace();
        } catch (IotServerException e) {
            System.err.println(e.getCode());
            e.printStackTrace();
        }
    }

    /**
     * 异步调用设备详情API
     */
    @Test
    public void testQueryDeviceDetailAsync() throws InterruptedException {
        CountDownLatch latch = new CountDownLatch(1);
        QueryDeviceDetailRequest request = new QueryDeviceDetailRequest();
        request.setProductId("5POpr2bk2g");
        request.setDeviceName("861658064469516");

        client.sendRequestAsync(request).whenComplete((response, cause) -> {
            if (response != null) {
                System.out.println(JSON.toJSONString(response));
            } else {
                if (cause instanceof IotServerException) {
                    IotServerException serverError = (IotServerException) cause;
                    System.err.println(serverError.getCode());
                }
                cause.printStackTrace();
            }
            latch.countDown();
        });
        latch.await();
    }

    /**
     * 同步调用设备状态历史数据查询API
     */
    @Test
    public void testQueryDeviceStatusHistory() {
        QueryDeviceStatusHistoryRequest request = new QueryDeviceStatusHistoryRequest();
        request.setProductId(productId);
        request.setDeviceName(deviceId);

        LocalDateTime tenDaysBefore = LocalDateTime.now().minus(Duration.ofDays(10));
        LocalDateTime now = LocalDateTime.now();
        request.setStartTime(Date.from(tenDaysBefore.toInstant(ZoneOffset.of("+8"))));
        request.setEndTime(Date.from(now.toInstant(ZoneOffset.of("+8"))));

        try {
            QueryDeviceStatusHistoryResponse response = client.sendRequest(request);
            System.out.println(JSON.toJSONString(response));
        } catch (IotClientException e) {
            e.printStackTrace();
        } catch (IotServerException e) {
            System.err.println(e.getCode());
            e.printStackTrace();
        }
    }

    /**
     * 异步调用设备状态历史数据查询API
     */
    @Test
    public void testQueryDeviceStatusHistoryAsync() throws InterruptedException {
        CountDownLatch latch = new CountDownLatch(1);
        QueryDeviceStatusHistoryRequest request = new QueryDeviceStatusHistoryRequest();
        request.setProductId(productId);
        request.setDeviceName(deviceId);
        LocalDateTime tenDaysBefore = LocalDateTime.now().minus(Duration.ofDays(10));
        LocalDateTime now = LocalDateTime.now();
        request.setStartTime(Date.from(tenDaysBefore.toInstant(ZoneOffset.of("+8"))));
        request.setEndTime(Date.from(now.toInstant(ZoneOffset.of("+8"))));

        client.sendRequestAsync(request).whenComplete((response, cause) -> {
            if (response != null) {
                System.out.println(JSON.toJSONString(response));
            } else {
                if (cause instanceof IotServerException) {
                    IotServerException serverError = (IotServerException) cause;
                    System.err.println(serverError.getCode());
                }
                cause.printStackTrace();
            }
            latch.countDown();
        });
        latch.await();
    }

    /**
     * 同步调用设备属性设置API
     */
    @Test
    public void testSetDeviceProperty() {
        SetDevicePropertyRequest request = new SetDevicePropertyRequest();
        request.setProductId(productId);
        request.setDeviceName(deviceId);
        request.addParam("asr", 1);

        try {
            SetDevicePropertyResponse response = client.sendRequest(request);
            System.out.println(JSON.toJSONString(response));
        } catch (IotClientException e) {
            e.printStackTrace();
        } catch (IotServerException e) {
            System.err.println(e.getCode());
            e.printStackTrace();
        }
    }

    /**
     * 异步调用设备属性设置API
     */
    @Test
    public void testSetDevicePropertyAsync() throws InterruptedException {
        CountDownLatch latch = new CountDownLatch(1);
        SetDevicePropertyRequest request = new SetDevicePropertyRequest();
        request.setProductId(productId);
        request.setDeviceName(deviceId);
        request.addParam("asr", 0);

        client.sendRequestAsync(request).whenComplete((response, cause) -> {
            if (response != null) {
                System.out.println(JSON.toJSONString(response));
            } else {
                if (cause instanceof IotServerException) {
                    IotServerException serverError = (IotServerException) cause;
                    System.err.println(serverError.getCode());
                }
                cause.printStackTrace();
            }
            latch.countDown();
        });
        latch.await();
    }

    /**
     * 同步调用设备属性期望设置API
     */
    @Test
    public void testSetDeviceDesiredPropertyResponse() {
        SetDeviceDesiredPropertyRequest request = new SetDeviceDesiredPropertyRequest();
        request.setProductId(productId);
        request.setDeviceName(deviceId);
        request.addParam("asr", 0);

        try {
            SetDeviceDesiredPropertyResponse response = client.sendRequest(request);
            System.out.println(JSON.toJSONString(response));
        } catch (IotClientException e) {
            e.printStackTrace();
        } catch (IotServerException e) {
            System.err.println(e.getCode());
            e.printStackTrace();
        }
    }

    /**
     * 异步调用设备属性期望设置API
     */
    @Test
    public void testSetDeviceDesiredPropertyResponseAsync() throws InterruptedException {
        CountDownLatch latch = new CountDownLatch(1);
        SetDeviceDesiredPropertyRequest request = new SetDeviceDesiredPropertyRequest();
        request.setProductId(productId);
        request.setDeviceName(deviceId);
        request.addParam("asr", 0);

        client.sendRequestAsync(request).whenComplete((response, cause) -> {
            if (response != null) {
                System.out.println(JSON.toJSONString(response));
            } else {
                if (cause instanceof IotServerException) {
                    IotServerException serverError = (IotServerException) cause;
                    System.err.println(serverError.getCode());
                }
                cause.printStackTrace();
            }
            latch.countDown();
        });
        latch.await();
    }

    /**
     * 同步调用设备属性期望查询API
     */
    @Test
    public void testQueryDeviceDesiredProperty() {
        QueryDeviceDesiredPropertyRequest request = new QueryDeviceDesiredPropertyRequest();
        request.setProductId(productId);
        request.setDeviceName(deviceId);
        request.addParam("asr");

        try {
            QueryDeviceDesiredPropertyResponse response = client.sendRequest(request);
            System.out.println("requestId:" + response.getRequestId());
            response.forEach((identify, property) -> {
                System.out.println("identify:" + identify);
                System.out.println("property:" + JSON.toJSONString(property));
            });
        } catch (IotClientException e) {
            e.printStackTrace();
        } catch (IotServerException e) {
            System.err.println(e.getCode());
            e.printStackTrace();
        }
    }

    /**
     * 异步调用设备属性期望查询API
     */
    @Test
    public void testQueryDeviceDesiredPropertyAsync() throws InterruptedException {
        CountDownLatch latch = new CountDownLatch(1);
        QueryDeviceDesiredPropertyRequest request = new QueryDeviceDesiredPropertyRequest();
        request.setProductId(productId);
        request.setDeviceName(deviceId);
        request.addParam("asr");

        client.sendRequestAsync(request).whenComplete((response, cause) -> {
            if (response != null) {
                System.out.println("requestId:" + response.getRequestId());
                response.forEach((identify, property) -> {
                    System.out.println("identify:" + identify);
                    System.out.println("property:" + JSON.toJSONString(property));
                });
            } else {
                if (cause instanceof IotServerException) {
                    IotServerException serverError = (IotServerException) cause;
                    System.err.println(serverError.getCode());
                }
                cause.printStackTrace();
            }
            latch.countDown();
        });
        latch.await();
    }

    /**
     * 同步调用设备属性期望删除API
     */
    @Test
    public void testDeleteDeviceDesiredProperty() {
        DeleteDeviceDesiredPropertyRequest request = new DeleteDeviceDesiredPropertyRequest();
        request.setProductId(productId);
        request.setDeviceName(deviceId);
        request.addParam("asr", 1);

        try {
            DeleteDeviceDesiredPropertyResponse response = client.sendRequest(request);
            System.out.println(JSON.toJSONString(response));
        } catch (IotClientException e) {
            e.printStackTrace();
        } catch (IotServerException e) {
            System.err.println(e.getCode());
            e.printStackTrace();
        }
    }

    /**
     * 异步调用设备属性期望删除API
     */
    @Test
    public void testDeleteDeviceDesiredPropertyAsync() throws InterruptedException {
        CountDownLatch latch = new CountDownLatch(1);
        DeleteDeviceDesiredPropertyRequest request = new DeleteDeviceDesiredPropertyRequest();
        request.setProductId(productId);
        request.setDeviceName(deviceId);
        request.addParam("asr");

        client.sendRequestAsync(request).whenComplete((response, cause) -> {
            if (response != null) {
                System.out.println(JSON.toJSONString(response));
            } else {
                if (cause instanceof IotServerException) {
                    IotServerException serverError = (IotServerException) cause;
                    System.err.println(serverError.getCode());
                }
                cause.printStackTrace();
            }
            latch.countDown();
        });
        latch.await();
    }

    /**
     * 同步调用设备操作日志查询API
     */
    @Test
    public void testQueryDeviceLog() {
        QueryDeviceLogRequest request = new QueryDeviceLogRequest();
        request.setProductId(productId);
        request.setDeviceName(deviceId);
        LocalDateTime tenDaysBefore = LocalDateTime.now().minus(Duration.ofDays(1));
        LocalDateTime now = LocalDateTime.now();
        request.setStartTime(Date.from(tenDaysBefore.toInstant(ZoneOffset.of("+8"))));
        request.setEndTime(Date.from(now.toInstant(ZoneOffset.of("+8"))));
        request.setOffset(0);
        request.setLimit(10);

        try {
            QueryDeviceLogResponse response = client.sendRequest(request);
            System.out.println(JSON.toJSONString(response));
        } catch (IotClientException e) {
            e.printStackTrace();
        } catch (IotServerException e) {
            System.err.println(e.getCode());
            e.printStackTrace();
        }
    }

    /**
     * 异步调用设备操作日志查询API
     */
    @Test
    public void testQueryDeviceLogAsync() throws InterruptedException {
        CountDownLatch latch = new CountDownLatch(1);
        QueryDeviceLogRequest request = new QueryDeviceLogRequest();
        request.setProductId(productId);
        request.setDeviceName(deviceId);
        LocalDateTime tenDaysBefore = LocalDateTime.now().minus(Duration.ofDays(1));
        LocalDateTime now = LocalDateTime.now();
        request.setStartTime(Date.from(tenDaysBefore.toInstant(ZoneOffset.of("+8"))));
        request.setEndTime(Date.from(now.toInstant(ZoneOffset.of("+8"))));
        request.setOffset(0);
        request.setLimit(10);

        client.sendRequestAsync(request).whenComplete((response, cause) -> {
            if (response != null) {
                System.out.println(JSON.toJSONString(response));
            } else {
                if (cause instanceof IotServerException) {
                    IotServerException serverError = (IotServerException) cause;
                    System.err.println(serverError.getCode());
                }
                cause.printStackTrace();
            }
            latch.countDown();
        });
        latch.await();
    }

    /**
     * 同步调用设备属性最新数据查询API
     */
    @Test
    public void testQueryDeviceProperty() {
        QueryDevicePropertyRequest request = new QueryDevicePropertyRequest();
        request.setProductId(productId);
        request.setDeviceName(deviceId);

        try {
            QueryDevicePropertyResponse response = client.sendRequest(request);
            System.out.println("requestId:" + response.getRequestId());
            response.forEach(deviceProperty -> System.out.println(JSON.toJSONString(deviceProperty)));
        } catch (IotClientException e) {
            e.printStackTrace();
        } catch (IotServerException e) {
            System.err.println(e.getCode());
            e.printStackTrace();
        }
    }

    /**
     * 异步调用设备属性最新数据查询API
     */
    @Test
    public void testQueryDevicePropertyAsync() throws InterruptedException {
        CountDownLatch latch = new CountDownLatch(1);
        QueryDevicePropertyRequest request = new QueryDevicePropertyRequest();
        request.setProductId(productId);
        request.setDeviceName(deviceId);

        client.sendRequestAsync(request).whenComplete((response, cause) -> {
            if (response != null) {
                System.out.println("requestId:" + response.getRequestId());
                response.forEach(deviceProperty -> System.out.println(JSON.toJSONString(deviceProperty)));
            } else {
                if (cause instanceof IotServerException) {
                    IotServerException serverError = (IotServerException) cause;
                    System.err.println(serverError.getCode());
                }
                cause.printStackTrace();
            }
            latch.countDown();
        });
        latch.await();
    }

    /**
     * 同步调用设备属性历史数据查询API
     */
    @Test
    public void testQueryDevicePropertyHistory() {
        QueryDevicePropertyHistoryRequest request = new QueryDevicePropertyHistoryRequest();
        request.setProductId(productId);
        request.setDeviceName(deviceId);

        request.setIdentifier(propertyId);

        LocalDateTime sixDaysBefore = LocalDateTime.now().minus(Duration.ofDays(6));
        LocalDateTime now = LocalDateTime.now();
        request.setStartTime(Date.from(sixDaysBefore.toInstant(ZoneOffset.of("+8"))));
        request.setEndTime(Date.from(now.toInstant(ZoneOffset.of("+8"))));
        request.setOffset(0);
        request.setLimit(10);
        request.setSort(Sort.DESC);

        try {
            QueryDevicePropertyHistoryResponse response = client.sendRequest(request);
            System.out.println(JSON.toJSONString(response));
        } catch (IotClientException e) {
            e.printStackTrace();
        } catch (IotServerException e) {
            System.err.println(e.getCode());
            e.printStackTrace();
        }
    }

    /**
     * 异步调用设备属性历史数据查询API
     */
    @Test
    public void testQueryDevicePropertyHistoryAsync() throws InterruptedException {
        CountDownLatch latch = new CountDownLatch(1);
        QueryDevicePropertyHistoryRequest request = new QueryDevicePropertyHistoryRequest();
        request.setProductId(productId);
        request.setDeviceName(deviceId);
        request.setIdentifier(propertyId);

        LocalDateTime tenDaysBefore = LocalDateTime.now().minus(Duration.ofDays(1));
        LocalDateTime now = LocalDateTime.now();
        request.setStartTime(Date.from(tenDaysBefore.toInstant(ZoneOffset.of("+8"))));
        request.setEndTime(Date.from(now.toInstant(ZoneOffset.of("+8"))));
        request.setOffset(0);
        request.setLimit(10);
        request.setSort(Sort.DESC);

        client.sendRequestAsync(request).whenComplete((response, cause) -> {
            if (response != null) {
                System.out.println(JSON.toJSONString(response));
            } else {
                if (cause instanceof IotServerException) {
                    IotServerException serverError = (IotServerException) cause;
                    System.err.println(serverError.getCode());
                }
                cause.printStackTrace();
            }
            latch.countDown();
        });
        latch.await();
    }

    /**
     * 同步调用设备事件历史数据查询API
     */
    @Test
    public void testQueryDeviceEventHistory() {
        QueryDeviceEventHistoryRequest request = new QueryDeviceEventHistoryRequest();
        request.setProductId(productId);
        request.setDeviceName(deviceId);
        request.setIdentifier(propertyId);

        request.setEventType(EventType.MESSAGE);
        LocalDateTime tenDaysBefore = LocalDateTime.now().minus(Duration.ofDays(1));
        LocalDateTime now = LocalDateTime.now();
        request.setStartTime(Date.from(tenDaysBefore.toInstant(ZoneOffset.of("+8"))));
        request.setEndTime(Date.from(now.toInstant(ZoneOffset.of("+8"))));
        request.setOffset(0);
        request.setLimit(10);

        try {
            QueryDeviceEventHistoryResponse response = client.sendRequest(request);
            System.out.println(JSON.toJSONString(response));
        } catch (IotClientException e) {
            e.printStackTrace();
        } catch (IotServerException e) {
            System.err.println(e.getCode());
            e.printStackTrace();
        }
    }

    /**
     * 异步调用设备事件历史数据查询API
     */
    @Test
    public void testQueryDeviceEventHistoryAsync() throws InterruptedException {
        CountDownLatch latch = new CountDownLatch(1);
        QueryDeviceEventHistoryRequest request = new QueryDeviceEventHistoryRequest();
        request.setProductId(productId);
        request.setDeviceName(deviceId);
        request.setIdentifier(propertyId);

        request.setEventType(EventType.MESSAGE);
        LocalDateTime tenDaysBefore = LocalDateTime.now().minus(Duration.ofDays(1));
        LocalDateTime now = LocalDateTime.now();
        request.setStartTime(Date.from(tenDaysBefore.toInstant(ZoneOffset.of("+8"))));
        request.setEndTime(Date.from(now.toInstant(ZoneOffset.of("+8"))));
        request.setOffset(0);
        request.setLimit(10);

        client.sendRequestAsync(request).whenComplete((response, cause) -> {
            if (response != null) {
                System.out.println(JSON.toJSONString(response));
            } else {
                if (cause instanceof IotServerException) {
                    IotServerException serverError = (IotServerException) cause;
                    System.err.println(serverError.getCode());
                }
                cause.printStackTrace();
            }
            latch.countDown();
        });
        latch.await();
    }
}
