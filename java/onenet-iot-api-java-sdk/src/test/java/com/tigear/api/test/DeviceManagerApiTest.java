package com.tigear.api.test;

import cm.heclouds.onenet.iot.api.entity.application.device.QueryDeviceDetailResponse;
import cm.heclouds.onenet.iot.api.entity.application.group.AddGroupDeviceResponse;
import cm.heclouds.onenet.iot.api.entity.application.group.QueryGroupDetailResponse;
import cm.heclouds.onenet.iot.api.entity.application.group.QueryGroupDeviceListResponse;
import cm.heclouds.onenet.iot.api.entity.application.group.RemoveGroupDeviceResponse;
import cm.heclouds.onenet.iot.api.exception.IotClientException;
import cm.heclouds.onenet.iot.api.exception.IotServerException;
import com.alibaba.fastjson.JSON;
import com.tigear.api.DeviceManagerApi;
import org.junit.Test;

import java.util.Collections;

public class DeviceManagerApiTest extends  BaseTest {


    private final String productId = "5POpr2bk2g";

    private final String deviceName = "861658064469516";

    private  final  String groupId = "ad736bed-f429-4040-aeba-6dc1ab05ca32";

    @Test
    public void testQueryDeviceDetail(){
        DeviceManagerApi deviceManagerApi = new DeviceManagerApi(oneNetIotClient);
        try {
            QueryDeviceDetailResponse queryDeviceDetailResponse = deviceManagerApi.queryDeviceDetail(productId, deviceName);
            System.out.println(JSON.toJSONString(queryDeviceDetailResponse));

        } catch (IotClientException e) {
            e.printStackTrace();
        } catch (IotServerException e) {
            System.err.println(e.getCode());
            e.printStackTrace();
        }
    }

    @Test
    public void testGroupDevice(){

        DeviceManagerApi deviceManagerApi = new DeviceManagerApi(oneNetIotClient);
        try {
            AddGroupDeviceResponse addGroupDeviceResponse = deviceManagerApi.addGroupDevice(groupId, productId, Collections.singletonList("861658064469516"));

            System.out.println(addGroupDeviceResponse.getRequestId());
            addGroupDeviceResponse.forEach(errorData -> System.out.println(JSON.toJSONString(errorData)));

            RemoveGroupDeviceResponse removeGroupDeviceResponse = deviceManagerApi.removeGroupDevice(groupId, productId, Collections.singletonList("861658064469516"));

            System.out.println(removeGroupDeviceResponse.getRequestId());
            removeGroupDeviceResponse.forEach(errorData -> System.out.println(JSON.toJSONString(errorData)));

        } catch (IotClientException e) {
            e.printStackTrace();
        } catch (IotServerException e) {
            System.err.println(e.getCode());
            e.printStackTrace();
        }
    }

    @Test
    public void testGroupDetail(){
        DeviceManagerApi deviceManagerApi = new DeviceManagerApi(oneNetIotClient);
        try {
            QueryGroupDetailResponse response = deviceManagerApi.queryGroupDetail(groupId);
            System.out.println(JSON.toJSONString(response));
        }
        catch (IotClientException e) {
            e.printStackTrace();
        } catch (IotServerException e) {
            System.err.println(e.getCode());
            e.printStackTrace();
        }
    }

    @Test
    public  void testGroupDeviceList(){
        DeviceManagerApi deviceManagerApi = new DeviceManagerApi(oneNetIotClient);
        try {
            QueryGroupDeviceListResponse response = deviceManagerApi.queryGroupDeviceList(groupId, 0, 10);
            System.out.println(JSON.toJSONString(response));
        }
        catch (IotClientException e) {
            e.printStackTrace();
        } catch (IotServerException e) {
            System.err.println(e.getCode());
            e.printStackTrace();
        }

    }

}
