package com.tigear.api.test;

import com.tigear.api.OneNetIotClient;
import org.junit.Before;

public class BaseTest {

    private final String userId = "your user id";
    private final String accessKey = "your access key";

    OneNetIotClient oneNetIotClient ;

    @Before
    public void init(){
        oneNetIotClient = new OneNetIotClient();
        oneNetIotClient.initClient(userId, accessKey);
    }


}
