package com.tigear.api.test;

import cm.heclouds.onenet.iot.api.entity.application.device.SetDevicePropertyResponse;
import cm.heclouds.onenet.iot.api.exception.IotClientException;
import cm.heclouds.onenet.iot.api.exception.IotServerException;
import com.alibaba.fastjson.JSON;
import com.tigear.api.DevicePropertyApi;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class DevicePropertyApiTest extends  BaseTest {

    private final String productId = "5POpr2bk2g";

    private final String deviceName = "861658064469516";

    @Test
    public void testSetDevicePropertyApi(){
        DevicePropertyApi devicePropertyApi = new DevicePropertyApi(oneNetIotClient);

        try {
            //设置单个
            SetDevicePropertyResponse response =   devicePropertyApi.setDeviceProperty(productId, deviceName, "asr", 0);
            System.out.println(JSON.toJSONString(response));

            //设置多个
            Map<String, Object> propertyValues = new HashMap<>();
            propertyValues.put("asr", 0);
            propertyValues.put("call_volume", 90);

            response =   devicePropertyApi.setDeviceProperty(productId, deviceName, propertyValues);
            System.out.println(JSON.toJSONString(response));

        } catch (IotClientException e) {
            e.printStackTrace();
        } catch (IotServerException e) {
            System.err.println(e.getCode());
            e.printStackTrace();
        }
    }

    @Test
    public void testQueryDevicePropertyValues(){
        DevicePropertyApi devicePropertyApi = new DevicePropertyApi(oneNetIotClient);

        try {
            HashMap<String, Object> propertyValues = devicePropertyApi.queryDevicePropertyValues(productId, deviceName, new String[]{"asr", "call_volume"});
            System.out.println(JSON.toJSONString(propertyValues));
        }
        catch (IotClientException e) {
            e.printStackTrace();
        } catch (IotServerException e) {
            System.err.println(e.getCode());
            e.printStackTrace();
        }
    }

}
