package com.tigear.onenethttpsvc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OnenetHttpSvcApplication {

    public static void main(String[] args) {
        SpringApplication.run(OnenetHttpSvcApplication.class, args);
    }

}
