# 工程说明

* 该样例工程为OneNET物联网开放平台平台HTTP推送Java例程， 基于该例程可快速了解进行通过HTTP方式对接OneNET物联网开放平台
* 在使用之前，请先在OneNET物联网开放平台中设置HTTP推送URL及token参数，请使用明文推送， 并修改例子代码中checkToken函数中对应的token值
* 该例程为明文推送例程

