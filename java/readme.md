# OneNET物联网开放平台应用接入SDK(JAVA)
钛极智能呼叫器网关支持通过中国移动OneNET物联网开放平台与第三方平台进行设备对接，方便用户快速将设备接入到自己的应用平台中。 
本SDK提供了JAVA语言的相关SDK及例程。

注意： 请使用JAVA开发平台 [IntelliJ IDEA](https://www.jetbrains.com/zh-cn/idea/) 来打开样例工程

相关目录如下：

| 目录                    | 说明                                     |
| ----------------------- | ---------------------------------------- |
| dist                    | OneNET平台SDK JAR包，用户可直接使用JAR包 |
| onenet-iotapi-sample    | OneNET平台SDK API例程                    |
| onenet-http-push        | OneNET平台HTTP推送例程                   |
| onenet-iot-api-java-sdk | OneNET平台SDK源码                        |
