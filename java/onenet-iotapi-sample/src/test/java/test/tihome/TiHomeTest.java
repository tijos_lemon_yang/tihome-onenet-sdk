package test.tihome;

import cm.heclouds.onenet.iot.api.exception.IotClientException;
import cm.heclouds.onenet.iot.api.exception.IotServerException;
import com.tigear.tihome.TiHome;
import com.tigear.tihome.domain.FamilyNumber;
import com.tigear.tihome.domain.FunctionKey;
import com.tigear.tihome.domain.SubDevice;
import com.tigear.tihome.type.ActionType;
import com.tigear.tihome.type.DeviceType;
import com.tigear.tihome.type.FunctionType;
import com.tigear.tihome.type.PhoneType;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class TiHomeTest {

    TiHome tiHome = new TiHome("your user id", "your access key",
            "5POpr2bk2g", "861658064504684");

    @Test
    public  void testUrgentContacts(){

        List<String> contacts = new ArrayList<>();
        contacts.add(new String("13801234567"));

        try {
            tiHome.setUrgentContacts(contacts);

            tiHome.getUrgentContacts();

        } catch (IotClientException e) {
            e.printStackTrace();
        } catch (IotServerException e) {
            System.err.println(e.getCode());
            e.printStackTrace();
        }
    }

    @Test
    public void testFamilyNumber(){

        FamilyNumber familyNumber = new FamilyNumber("13801234567", PhoneType.None);

        List<FamilyNumber> familyNumbers = new ArrayList<>();
        familyNumbers.add(familyNumber);

        try{
            tiHome.setFamilyNumbers(familyNumbers);

            tiHome.getFamilyNumbers();

        } catch (
        IotClientException e) {
            e.printStackTrace();
        } catch (IotServerException e) {
            System.err.println(e.getCode());
            e.printStackTrace();
        }
    }

    @Test
    public void testGetSubDevices(){

        try{
            SubDevice subDevice = new SubDevice("10000503224F0600", DeviceType.SOSButton, ActionType.NONE, 0, 1);
            List<SubDevice> subDevices = tiHome.getSubDevices();

            for(SubDevice subDev : subDevices) {
                System.out.println(subDev.toJsonObject().toJSONString());
            }

        } catch (IotClientException e) {
            e.printStackTrace();
        } catch (IotServerException e) {
            System.err.println(e.getCode());
            e.printStackTrace();
        }
    }

    @Test
    public  void testAddSubDevice(){
        try{
            SubDevice subDevice = new SubDevice("10000503224F0600", DeviceType.SOSButton, ActionType.NONE, 0, 1);
            tiHome.addSubDevice(subDevice);

        } catch (IotClientException e) {
            e.printStackTrace();
        } catch (IotServerException e) {
            System.err.println(e.getCode());
            e.printStackTrace();
        }
    }

    @Test
    public  void testDelSubDevice(){
        try{
            tiHome.deleteSubDevice("10000503224F0600");

        } catch (IotClientException e) {
            e.printStackTrace();
        } catch (IotServerException e) {
            System.err.println(e.getCode());
            e.printStackTrace();
        }
    }


    @Test
    public void testServicePunch(){

        try{
            tiHome.servicePunchStart(30);

            tiHome.servicePunchFinish("1234");

        }catch (IotClientException e) {
            e.printStackTrace();
        } catch (IotServerException e) {
            System.err.println(e.getCode());
            e.printStackTrace();
        }
    }

    @Test
    public void testTTSPlay(){

        try{
            tiHome.ttsPlay("这是一个测试");

        }catch (IotClientException e) {
            e.printStackTrace();
        } catch (IotServerException e) {
            System.err.println(e.getCode());
            e.printStackTrace();
        }
    }

    @Test
    public void testVolume(){

        try{
            tiHome.setAudioVolume(60);

            int vol = tiHome.getAudioVolume();
            System.out.println("volume " + vol);

        }catch (IotClientException e) {
            e.printStackTrace();
        } catch (IotServerException e) {
            System.err.println(e.getCode());
            e.printStackTrace();
        }
    }

    @Test
    public void testLeaveMessageAdd(){

        try{
            tiHome.leaveMessageAdd("msg1", "http://img.tijos.net/img/msg1.amr", 0);

        }catch (IotClientException e) {
            e.printStackTrace();
        } catch (IotServerException e) {
            System.err.println(e.getCode());
            e.printStackTrace();
        }
    }

    @Test
    public void testSetFunctionKeys(){
        try{
            //按键1 播放留言
            FunctionKey key1 = new FunctionKey(FunctionType.PLAY_MESSAGE, 0);

            //按键2 挂断电话
            FunctionKey key2 = new FunctionKey(FunctionType.END_CALL, 0);

            tiHome.setFunctionKeys(key1, key2);


        }catch (IotClientException e) {
            e.printStackTrace();
        } catch (IotServerException e) {
            System.err.println(e.getCode());
            e.printStackTrace();
        }
    }
}
