package test.api;

import cm.heclouds.onenet.iot.api.entity.application.device.QueryDeviceDetailResponse;
import cm.heclouds.onenet.iot.api.entity.application.group.AddGroupDeviceResponse;
import cm.heclouds.onenet.iot.api.entity.application.group.RemoveGroupDeviceResponse;
import cm.heclouds.onenet.iot.api.exception.IotClientException;
import cm.heclouds.onenet.iot.api.exception.IotServerException;
import com.alibaba.fastjson.JSON;
import com.tigear.api.DeviceManagerApi;
import org.junit.Test;

import java.util.Collections;

/**
 * 设备管理API
 */
public class DeviceManagerApiTest extends BaseTest {

    private final String productId = "5POpr2bk2g";

    private final String deviceName = "861658064469516";


    /**
     * 获取设备信息
     */
    @Test
    public void testQueryDeviceDetail(){
        DeviceManagerApi deviceManagerApi = new DeviceManagerApi(oneNetIotClient);
        try {
            QueryDeviceDetailResponse queryDeviceDetailResponse = deviceManagerApi.queryDeviceDetail(productId, deviceName);
            System.out.println(JSON.toJSONString(queryDeviceDetailResponse));

        } catch (IotClientException e) {
            e.printStackTrace();
        } catch (IotServerException e) {
            System.err.println(e.getCode());
            e.printStackTrace();
        }
    }

    /**
     * 从设备分组增加删除设备
     */
    @Test
    public void testGroupDevice(){

        String groupId = "b44c0bab-b41c-4bd5-a930-6cdb0ea9b8da";
        DeviceManagerApi deviceManagerApi = new DeviceManagerApi(oneNetIotClient);
        try {
            AddGroupDeviceResponse addGroupDeviceResponse = deviceManagerApi.addGroupDevice(groupId, productId, Collections.singletonList(deviceName));

            System.out.println(addGroupDeviceResponse.getRequestId());
            addGroupDeviceResponse.forEach(errorData -> System.out.println(JSON.toJSONString(errorData)));

            RemoveGroupDeviceResponse removeGroupDeviceResponse = deviceManagerApi.removeGroupDevice(groupId, productId, Collections.singletonList(deviceName));

            System.out.println(removeGroupDeviceResponse.getRequestId());
            removeGroupDeviceResponse.forEach(errorData -> System.out.println(JSON.toJSONString(errorData)));

        } catch (IotClientException e) {
            e.printStackTrace();
        } catch (IotServerException e) {
            System.err.println(e.getCode());
            e.printStackTrace();
        }

    }

}
