package test.api;

import cm.heclouds.onenet.iot.api.entity.application.device.CallServiceResponse;
import cm.heclouds.onenet.iot.api.exception.IotClientException;
import cm.heclouds.onenet.iot.api.exception.IotServerException;
import com.alibaba.fastjson.JSON;
import com.tigear.api.DeviceServiceApi;
import org.junit.Test;

import java.util.HashMap;

/**
 * 调用设备服务
 */
public class DeviceServiceApiTest extends  BaseTest {
    private final String productId = "5POpr2bk2g";

    private final String deviceName = "861658064469516";

    /**
     * 调用设备服务
     */
    @Test
    public void testCallService(){
        DeviceServiceApi deviceServiceApi = new DeviceServiceApi(oneNetIotClient);

        HashMap<String, Object> serviceParams = new HashMap<String, Object>();
        serviceParams.put("timeout", 30);

        try {
            CallServiceResponse result = deviceServiceApi.callService(productId, deviceName, "service_punch_start", serviceParams);
            System.out.println("requestId " + result.getRequestId());

            System.out.println(JSON.toJSONString(result));


        } catch (IotClientException e) {
            e.printStackTrace();
        } catch (IotServerException e) {
            System.err.println(e.getCode());
            e.printStackTrace();
        }




    }
}
