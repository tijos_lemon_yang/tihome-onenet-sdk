package test.api;

import com.tigear.api.OneNetIotClient;
import org.junit.Before;

public class BaseTest {

    /**
     * 改为您自己OneNET账号下的userId和accesskey
     * 上课威威OneNet当前用户账号信息下的访问权限中获得
     */
    private final String userId = "your user id";
    private final String accessKey = "your access key";

    OneNetIotClient oneNetIotClient ;

    @Before
    public void init(){
        oneNetIotClient = new OneNetIotClient();
        oneNetIotClient.initClient(userId, accessKey);
    }


}
