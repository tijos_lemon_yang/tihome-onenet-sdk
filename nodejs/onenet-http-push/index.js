const express = require('express')
const app = express()

app.use(express.json())

//onenet 验证
app.get('/onenet/receive', (req, res)=>{
    console.log(req.query)
    res.send(req.query.msg)
})

//onenet 数据推送
app.post('/onenet/receive',(req, res)=>{
    console.log(req.body.msg)

    let onenetMsg = JSON.parse(req.body.msg)
    console.log(onenetMsg)

    res.send('ok')
})

app.listen(8080, ()=>{
    console.log('listening on 8080 port.')
})

