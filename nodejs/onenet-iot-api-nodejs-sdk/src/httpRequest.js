const axios = require("axios");
const QS = require('querystring');

function get(params) {
    let conUrl = params.url
    const url = conUrl + '/' + params.path + '/' + params.query.action + '?' + QS.stringify(params.body)
    let header = {
        'authorization': params.authorization
    };
    return httpRequest("get", url, header);
}

function post(params) {
    let conUrl = params.url
    const url = conUrl + '/' + params.path + '/' + params.query.action;
    let header = {
        'authorization': params.authorization,
        'Content-Type': 'application/json'
    };
    return httpRequest("post", url, header, params.body);
}

function httpRequest(method, url, header, data) {
    console.log(method, url, header, data);

    return new Promise((resolve, reject) => {
        axios({
            headers: header,
            method: method,
            url: url,
            data: data
        })
            .then(res => {
                resolve(res.data)
            })
            .catch(err => {
                reject(err)
            })
    })

}

module.exports = {
    get,
    post
};