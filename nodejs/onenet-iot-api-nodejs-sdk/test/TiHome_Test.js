const { group } = require("console");

TiHome = require("../src/TiHome.js")

const userId = "your user id"
const accessKey = "your access key";

const productId = "5POpr2bk2g";
const deviceName = "861658064504684";

const groupId =  "6adfde33-8e1a-455d-b74c-fafed43862ae";


var tihome = new TiHome(userId, accessKey, productId, deviceName);

//testGetDeviceStatus();
//testAddToGroup();
//testRemoveFromGroup();
//testSetUrgentContacts();
//testSetFamilyNumbers();
//testSetAudioVolume();
//testGetSubDevices();
//testAddSubDevice();
//testServicePunch();
//testTTS();
//testLeaveMessageAdd();

testTimerTasks();


async function testGetDeviceStatus(){
    var resp = await tihome.getDeviceStatus();

    console.log(resp);
}

async function testAddToGroup(){
    
    var resp = await tihome.addToGroup(groupId);    
    console.log(resp);
}

async function testRemoveFromGroup(){
    var resp = await tihome.removeFromGroup(groupId);
    console.log(resp);
}

async function testSetUrgentContacts(){
    
    let contacts = ["13812341234", "18612341234"];

    var resp = await tihome.setUrgentContacts(contacts);
    console.log(resp);

    resp = await tihome.getUrgentContacts();
    console.log(resp);

}

async function testSetFamilyNumbers() {

    let familyNumbers = [
        {phone: "13812341234", "type" : 0}, 
        {phone: "18612341234", "type" : 1},
    ];

    var resp = await tihome.setFamilyNumbers(familyNumbers);
    console.log(resp);

    resp = await tihome.getFamilyNumbers();
    console.log(resp);
}

async function testSetAudioVolume() {

    var resp = await tihome.setAudioVolume(80);

    console.log(resp);

    resp = await tihome.getAudioVolume();
    console.log(resp);
    if(resp.code == 0) {
      console.log("volume :", resp.data.volume);
    }
}

async function testGetSubDevices(){

    var resp = await tihome.getSubDevices();
    console.log(resp);

    if(resp.code == 0) {
        resp.data.subdevices.forEach(element => {
            console.log(element);            
        });
    }
}

async function testAddSubDevice(){
    let subdev = {
        uid :'1000030353F40600',
        devtype: 16,
        audible: 1,
        action: 0,
        param: 0
    };

    var resp = await tihome.addSubDevice(subdev);
    console.log(resp);

    resp = await tihome.changeSubDevice(subdev);
    console.log(resp);

    resp = await tihome.deleteSubDevice(subdev.uid);
    console.log(resp);

}

async function testServicePunch(){
    var resp = await tihome.servicePunchStart(30);
    console.log(resp);

    resp = await tihome.servicePunchFinish("1234");
    console.log(resp);
}

async function testTTS() {
    var resp = await tihome.ttsPlay("测试测试");
    console.log(resp);
}

async function testLeaveMessageAdd(){
    var resp = await tihome.leaveMessageAdd("msg1", "http://img.tijos.net/img/msg1.amr", 0);
    console.log(resp);
}

async function testTimerTasks(){
    let tasks = [{
        hour: 9,
        min: 40,
        week: 0xFF,
        text:"546w5Zyo5piv6Z+z5LmQ5pe26Ze0",
        disable: 0,
        url:"",
        times: 0
    },
    {
        hour: 9,
        min: 30,
        week: 0xFF,
        text:"546w5Zyo5piv6Z+z5LmQ5pe26Ze0",
        disable: 0,
        url:"",
        times: 0
    }
    ];

    var resp = await tihome.setTimerTasks(tasks);
    console.log(resp);

    resp = await tihome.getTimerTasks();
    console.log(resp);


}

