
API = require("../src/HttpApi")

console.log("start ....")

const userId = "your user id";
const accessKey = "your access key";

var api = new API(userId, accessKey)

//testCreateDevice()
//testDeleteDevice()

testQueryDevice()
//testGroupAddDevice()
//testGroupRemoveDevice()
//testSetDeviceProperty()
//testQueryDeviceProperty()
//testCallService()

async function testCreateDevice(){
    const response = await api.CreateDevice("5POpr2bk2g", "deviceName", "mydevice")
    console.log(response)
}

async function testDeleteDevice(){
    const response = await api.DeleteDevice("5POpr2bk2g", "deviceName", "mydevice");
    console.log(response)
}

async function testQueryDevice(){
    const response = await api.QueryDeviceDetail("5POpr2bk2g","865328061412499");
    console.log(response)
}

async function testGroupAddDevice(){

    const response = await api.GroupAddDevice("6adfde33-8e1a-455d-b74c-fafed43862ae", "5POpr2bk2g","865328061412499")
    console.log(response)

}

async function testGroupRemoveDevice(){
    const response = await api.GroupRemoveDevice("6adfde33-8e1a-455d-b74c-fafed43862ae", "5POpr2bk2g","865328061412499")
    console.log(response)

}

async function testSetDeviceProperty(){
    const response = await api.SetDeviceProperty("5POpr2bk2g","865328061412499", "asr", 0) 
    console.log(response)   
}

async function testQueryDeviceProperty(){
    const response = await api.QueryDeviceProperty("5POpr2bk2g","865328061412499", "asr") 
    console.log(response)   

}

async function testCallService(){
    const response = await api.CallDeviceService("5POpr2bk2g","865328061412499", "update_all", {cmd:0}) 
    console.log(response)   

}