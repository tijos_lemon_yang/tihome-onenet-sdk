
const QS = require('querystring')

function get(params) {
  let conUrl = params.url
  const url = conUrl + '/' + params.path + '/' + params.query.action + '?' + QS.stringify(params.body)
  let header = {
    'authorization' : params.authorization
  };
  return httpRequest("GET", url, header);
}

function post(params) {
  let conUrl = params.url
  const url = conUrl + '/' + params.path + '/' + params.query.action;
  let header = {
    'authorization': params.authorization,
    'Content-Type': 'application/json'
  };
  return httpRequest("POST", url, header, params.body);
}

function httpRequest(method, url, header, data){
  console.log(method, url, header,  data);
  
  let pro = new Promise(function(resolve, reject) {
    wx.request({
      url: url,
      header: header,
      data: data,
      method: method,
      success:function(res) {
          console.log('success', res.data);
          if(res.statusCode == 200) {
            resolve(res.data);
          }else{
            reject(res.data);
          }
      },
      fail:function(res){
        console.log('fail', res.data);
        reject(res.data);
      },
      complete:function(res){
        console.log('complete' , res.data);
      }
    });
  });
  return pro;
}

module.exports = {
  get,
  post
};