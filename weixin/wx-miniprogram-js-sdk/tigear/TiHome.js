const HttpApi = require("./HttpApi.js")

/**
 * TiHome 系列产品OneNET开放平台API 
 * HTTP返回格式 
 *  code :  0 成功  其它 错误码
 *  msg  :  错误信息
 *  data :  返回数据
 *  requestId:  请求ID, 异步调用时通过HTTP推送返回结果 
 */
class TiHome {

    /**
     * 初始化
     * @param {string} userId      用户ID
     * @param {string} accessKey   用户KEY
     * @param {string} productId   产品ID
     * @param {string} deviceName  设备名称
     */
    constructor(userId, accessKey, productId, deviceName) {
        this.api = new HttpApi(userId, accessKey);
        this.productId = productId;
        this.deviceName = deviceName;
    }

    /**
     * 产品ID
     * @param {string} productId 
     */
    setProductId(productId){
      this.productId = productId;
  }

  /**
   * 产品名称
   * @param {string} deviceName 
   */
  setDeviceName(deviceName){
      this.deviceName =deviceName;
  }

    /**
     * 获取设备状态
     * @returns {Object} response HTTP 响应   data中为设备信息及状态
     */
    async getDeviceStatus() {
        var response = await this.api.QueryDeviceDetail(this.productId, this.deviceName);
        return response;
    }

    /**
     * 将设备加入到分组
     * @param {string} groupId
     * @returns {Object} response HTTP 响应
     */
    async addToGroup(groupId) {
        return await this.api.GroupAddDevice(groupId, this.productId, this.deviceName);
    }

    /**
     * 从分组中删除设备
     * @param {string} groupId 分组ID
     * @returns {Object} response HTTP 响应
     */
    async removeFromGroup(groupId) {
        return await this.api.GroupRemoveDevice(groupId, this.productId, this.deviceName);
    }

    /**
     * 设备属性值
     * @param {string} propertyName  属性名称
     * @param {any} propertyValue  属性值
     * @returns {Object} response HTTP 响应
     */
    async setPropertyValue(propertyName, propertyValue) {
        return await this.api.SetDeviceProperty(this.productId, this.deviceName, propertyName, propertyValue);
    }

    /**
     * 获取属性值
     * @param {string} propertyName   属性名称
     * @returns {Object} response HTTP 响应， data中包含了属性值
     */
    async getPropertyValue(propertyName) {
        var response = await this.api.QueryDeviceProperty(this.productId, this.deviceName, propertyName);
        return response;
    }

    /**
     * 调用服务
     * @param {string} serviceName  服务名称
     * @param {Object} serviceParams 服务参数
     * @returns {Object} response HTTP 响应
     */
    async callService(serviceName, serviceParams) {
        var response = await this.api.CallDeviceService(this.productId, this.deviceName, serviceName, serviceParams);
        return response;
    }

    /**
     * 重启设备
     * @returns {Object} response HTTP 响应
     */
    async reboot() {
        let param = { "cmd": 0 };
        await this.callService("reboot", param);
    }

    /**
     * 远程呼叫
     * @param {string} phone   电话号码， 如果为空字符串，将拔打紧急联系人电话
     * @param {string} text   语音提示， 拔打之前的语音提示
     * @returns {Object} response HTTP 响应
     */
    async remoteCall(phone, text) {
        let param = { "phone_number": phone, "text": text };
        var response = this.callService("remote_call", param);

        return response;
    }

    /**
     * 设置紧急联系人
     * @param {array} phoneNumbers   紧急联系人电话
     * @returns {Object} response HTTP 响应
     */
    async setUrgentContacts(phoneNumbers) {
        let contacts = "";

        for (var i = 0; i < phoneNumbers.length; i++) {
            contacts += phoneNumbers[i];
            if (i < phoneNumbers.length - 1) {
                contacts += ",";
            }
        }

        return await this.setPropertyValue("emergency_contact", contacts);
    }

    /**
     * 获取紧急联系人
     * @returns   {Object} response HTTP 响应
     */
    async getUrgentContacts(){
      return  await this.getPropertyValue("emergency_contact");
    }


    /**
     * 设备亲情联系人
     * @param familyNumbers   亲情联系人列表
     * @returns {Object} response HTTP 响应
     */
    async setFamilyNumbers(familyNumbers) {
        return await this.setPropertyValue("family_numbers", familyNumbers);
    }

     /**
     * 获取亲情联系人
     * @returns {Object} response HTTP 响应
     */
    async getFamilyNumbers(){
      return await this.getPropertyValue("family_numbers");
    }

    /**
     * 设置音量大小
     * @param {int} volume   0 - 100
     * @returns {Object} response HTTP 响应
     */
    async setAudioVolume(volume) {
        return this.setPropertyValue("volume", volume);
    }

    /**
     * 获取音量大小
     * @returns {Object} response HTTP 响应, data中包含音量值 
     */
    async getAudioVolume() {
        return this.getPropertyValue("volume");
    }
    /**
         * 获取定时任务
         * @returns {Object} response HTTP 响应
         */
        async getTimerTasks(){
          return this.getPropertyValue("timer_tasks");
      }

    /**
     * 设置定时任务
     * @param {array} tasks 
     * @returns 
     */
    async setTimerTasks(tasks) {
        return this.setPropertyValue("timer_tasks", tasks);
    }


    /**
     * 获取子设备列表
     * @returns {Object} response HTTP 响应  data中数据为子设备列表
     */
    async getSubDevices() {
        return await this.getPropertyValue("subdevices");
    }

    /**
     * 添加子设备
     * @param {Object} subDevice  子设备
     * @param {string} subDevice.uid  设备ID
     * @param {int} subDevice.devtype 设备类型
     * @param {int} subDevice.audible 报警提示 0 静音 1 语音提示
     * @param {int} subDevice.action 报警动作 0 无 1 拔紧急联系人 2 拔打亲情联系人 3 收听留言
     * @param {int} subDevice.param  报警参数 适用于拔打亲情联系人ID
     * @returns {Object} response HTTP 响应  
     */
    async addSubDevice(subDevice) {
        return await this.callService("subdevice_add", subDevice);
    }

    /**
     * 修改子设备
     * @param {Object} subDevice  子设备
     * @param {string} subDevice.uid  设备ID
     * @param {int} subDevice.audible 报警提示 0 静音 1 语音提示
     * @param {int} subDevice.action 报警动作 0 无 1 拔紧急联系人 2 拔打亲情联系人 3 收听留言
     * @param {int} subDevice.param  报警参数 适用于拔打亲情联系人ID
     * @returns {Object} response HTTP 响应  
     */
    async changeSubDevice(subDevice) {
        delete subDevice.devtype;
        return await this.callService("subdevice_change", subDevice);
    }

    /**
     * 删除子设备
     * @param {string} deviceSN  子设备ID
     * @returns {Object} response HTTP 响应  
     */
    async deleteSubDevice(deviceSN) {
        let param = { "uid": deviceSN };
        return await this.callService("subdevice_del", param);
    }

    /**
     * 开始服务打卡
     * @param {int} seconds  超时时间（秒）
     * @returns {Object} response HTTP 响应  
     */
    async servicePunchStart(seconds) {
        let param = { "timeout": seconds };
        return await this.callService("service_punch_start", param);
    }

    /**
     * 完成打卡
     * @param {string} code  打卡验证码
     * @returns {Object} response HTTP 响应， data中的result:  0 - 成功   1 未启动打卡  2 超时  3  验证码错误
     */
    async servicePunchFinish(code) {
        let param = { "punch_code": code };
        var response = await this.callService("service_punch_finish", param);
        return response;
    }

    /**
     * Text to Speech 文字转语音播放
     * @param {string} text 要播放的文字
     * @returns {Object} response HTTP 响应  
     */
    async ttsPlay(text) {
        let param = { "utf8_text": text };
        return await this.callService("tts_play_cmd", param);
    }

    /**
     * 语音留言
     * @param {string} mid   留言ID   字符串 长度<16
     * @param {string} url   音频文件url
     * @param {int} format   音频文件格式  0 amr  1 mp3
     * @returns {Object} response HTTP 响应  
     */
    async leaveMessageAdd(mid, url, format) {
        let param = { "mid": mid, "url": url, "format": format };
        return await this.callService("leave_message_add", param);
    }
}

module.exports = TiHome;