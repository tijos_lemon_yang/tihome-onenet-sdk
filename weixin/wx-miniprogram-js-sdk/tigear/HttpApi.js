const _httpRequest = require('./httpRequest.js')
const _key = require('./key.js')
/**
 * @方法集合
 * 设备管理类
 * 接口名称
 * 设备创建(单个)-----CreateDevice
 * 设备编辑-----UpdateDevice
 * 设备删除-----DeleteDevice
 * 设备详情-----QueryDeviceDetail
 * 
 * 设备分组操作类
 * 分组添加设备-----GroupAddDevice
 * 分组设备移除-----GroupRemoveDevice
 * 
 * 物模型操作类
 * 设备属性设置-----SetDeviceProperty
 * 设备属性最新数据查询-----QueryDeviceProperty
 * 服务调用 --- CallDeviceService
 */

class HttpApi {
  /**
   * 初始化
   * @param {string} userId   用户ID
   * @param {string} accessKey   用户KEY
   */
  constructor(userId, accessKey) {
    const config = {
      author_key : accessKey,
      user_id : userId,
      version : '2022-05-01',
      url : 'https://iot-api.heclouds.com',
    }

    this.url = config.url

    // 计算token
    const token = _key.createCommonToken(config)
    this.token = token
  }

  initParams(path, action, productId, deviceName) {
    const params = {} 
    params.authorization = this.token
    params.url = this.url
    params.path = path

    params.query = {}
    params.query.action = action

    params.body = {}
    params.body.product_id = productId
    params.body.device_name  = deviceName

    return params
  }

//API集合
  // 设备管理类

  /**
   * 创建设备
   * @param {string} productId 
   * @param {string} deviceName 
   * @param {string} desc 
   * @returns 
   */
  async CreateDevice(productId, deviceName, desc){
    const params = this.initParams('device', 'create', productId, deviceName)
    params.body.desc = desc

    return await _httpRequest.post(params)
  }

  /**
   * 删除设备
   * @param {string} productId 
   * @param {string} deviceName 
   * @returns 
   */
  async DeleteDevice(productId, deviceName){
    
    const params = this.initParams('device', 'delete', productId, deviceName)
    return await _httpRequest.post(params)
  }

  /**
   * 查询设备信息
   * @param {string} productId 
   * @param {string} deviceName 
   * @returns 
   */
  async QueryDeviceDetail(productId, deviceName){

    const params = this.initParams('device', 'detail', productId, deviceName)
    return await _httpRequest.get(params)
  }

  /**
   * 将设备加入到组
   * @param {string} groupId 
   * @param {string} productId 
   * @param {string} deviceName 
   * @returns 
   */
  async GroupAddDevice( groupId, productId, deviceName){

    const params = this.initParams('devicegroup', 'add-devices', productId)
    params.body.device_name_list = Array(deviceName)
    params.body.group_id = groupId;

    return await _httpRequest.post(params) 
  }

  /**
   * 从组中删除设备
   * @param {string} groupId 
   * @param {string} productId 
   * @param {string} deviceName 
   * @returns 
   */
  async GroupRemoveDevice(groupId, productId, deviceName){

    const params = this.initParams('devicegroup', 'del-devices', productId)
    params.body.device_name_list = Array(deviceName)
    params.body.group_id = groupId

    return await _httpRequest.post(params) 
  }

  /**
   *  设置设备属性值 
   * @param {string} productId 
   * @param {string} deviceName 
   * @param {string} propertyName 
   * @param {object} propertyValue 
   * @returns 
   */
  async SetDeviceProperty(productId, deviceName, propertyName, propertyValue){
    const params = this.initParams('thingmodel', 'set-device-property', productId, deviceName)

    params.body.params = {};
    params.body.params[propertyName] = propertyValue;

    return await _httpRequest.post(params) 
  }

  /**
   * 查询设备属性值
   * @param {string} productId 
   * @param {string} deviceName 
   * @param {string} propertyName 
   * @returns 
   */
  async QueryDeviceProperty(productId, deviceName, propertyName){

    const params = this.initParams('thingmodel', 'query-device-property-detail', productId, deviceName)
    params.body.params = new Array()
    params.body.params[0] = propertyName

    return await _httpRequest.post(params) 
  }
 
  /**
   * 调用设备服务
   * @param {string} productId 
   * @param {string} deviceName 
   * @param {string} serivceName 
   * @param {Map} serviceParams 
   * @returns 
   */
  async CallDeviceService(productId, deviceName, serivceName, serviceParams) {

    const params = this.initParams('thingmodel', 'call-service', productId, deviceName)
    params.body.identifier = serivceName
    params.body.params = serviceParams
    return await _httpRequest.post(params) 
  }
}

module.exports = HttpApi;