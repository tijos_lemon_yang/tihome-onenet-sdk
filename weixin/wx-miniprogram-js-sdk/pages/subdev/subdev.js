// pages/subdev/subdev.js

const tihome = getApp().globalData.tihome;

Page({

  /**
   * 页面的初始数据
   */
  data: {
     response : '',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {

  },

  async onButtonGetSubDeviceList(){
    var resp = await tihome.getSubDevices();
    console.log(resp);  
    if(resp.code == 0) {
        resp.data.subdevices.forEach(element => {
            console.log(element);            
        });
    }

    this.setData({
      response : JSON.stringify(resp)
    });

  },
  async onButtonAddSubDev(){
    let subdev = {
      uid :'1000030353F40600',
      devtype: 16,
      audible: 1,
      action: 0,
      param: 0
    };

    var resp = await tihome.addSubDevice(subdev);
    console.log(resp);
    this.setData({
      response : JSON.stringify(resp)
    });

  },
  async onButtonChangeSubDev(){
    let subdev = {
      uid :'1000030353F40600',
      audible: 1,
      action: 0,
      param: 0
    };
    var resp = await tihome.changeSubDevice(subdev);
    console.log(resp);
    this.setData({
      response : JSON.stringify(resp)
    });

  },
  async onButtonDeleteSubDev(){
    let uid = '1000030353F40600';
    var resp = await tihome.deleteSubDevice(uid);
    console.log(resp);
    this.setData({
      response : JSON.stringify(resp)
    });

  },


})