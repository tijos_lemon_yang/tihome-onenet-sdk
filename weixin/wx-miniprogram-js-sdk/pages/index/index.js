// index.js
// 获取应用实例
const app = getApp()
const tihome = app.globalData.tihome;

Page({
  data: {
    deviceName : '',
    response : '',
  },
  
  // 事件处理函数
  onLoad() {
    tihome.setDeviceName(this.data.deviceName);
  },

  onButtonScanCode(event){
    // 允许从相机和相册扫码
    self = this;
    wx.scanCode({
      success (res) {
        console.log(res)
        let deviceName =  res.result;
        self.setData({
          deviceName: deviceName
        });
        tihome.setDeviceName(deviceName);
      }
    })
  },

  onDeviceNameInput(event){
     let name = event.detail.value;
     this.setData({
       deviceName:  name
     });

     tihome.setDeviceName(name);
  },

  async onButtonGetDeviceInfo(){
    let resp = await tihome.getDeviceStatus()    
    this.setData({
        response : JSON.stringify(resp)
     })
  },

  async onButtonUrgentContact(){
    let contacts = ["13810314817", "18602648486"];
    let resp = await tihome.setUrgentContacts(contacts);
    this.setData({
      response : JSON.stringify(resp)
    });

    resp = await tihome.getUrgentContacts();
    this.setData({
      response : JSON.stringify(resp)
    });
  },
  async onButtonFamilyNumbers(){
    let familyNumbers = [
      {phone: "13810314817", "type" : 0}, 
      {phone: "18602648486", "type" : 1},
  ];
   let resp = await tihome.setFamilyNumbers(familyNumbers);
   this.setData({
    response : JSON.stringify(resp)
  });

   resp = await tihome.getFamilyNumbers();
   this.setData({
    response : JSON.stringify(resp)
  });
},

   async onButtonVolume(){
    let resp = await tihome.setAudioVolume(80);
    this.setData({
      response : JSON.stringify(resp)
    });
  
    resp = await tihome.getAudioVolume();
    this.setData({
      response : JSON.stringify(resp)
    });

    if(resp.code == 0) {
      console.log("volume :", resp.data.volume);
    }
   },
   async onButtonServicePunch(){
    let resp = await tihome.servicePunchStart(30);
    console.log(resp);
    this.setData({
      response : JSON.stringify(resp)
    });
    resp = await tihome.servicePunchFinish("1234");
    console.log(resp);
    this.setData({
      response : JSON.stringify(resp)
    });
   },

  async onButtonTimerTask(){
    let tasks = [{
            hour: 9,
            min: 40,
            week: 0xFF,
            text:"546w5Zyo5piv6Z+z5LmQ5pe26Ze0",
            disable: 0,
            url:"",
            times: 0
        },
        {
            hour: 9,
            min: 30,
            week: 0xFF,
            text:"546w5Zyo5piv6Z+z5LmQ5pe26Ze0",
            disable: 0,
            url:"",
            times: 0
        }
        ];
        var resp = await tihome.setTimerTasks(tasks);
        console.log(resp);
        this.setData({
          response : JSON.stringify(resp)
        });

        resp = await tihome.getTimerTasks();
        console.log(resp);
        this.setData({
          response : JSON.stringify(resp)
        });
  },


   onButtonSubDevices(){
      wx.navigateTo({
        url: '../subdev/subdev',
      })
   },



})
