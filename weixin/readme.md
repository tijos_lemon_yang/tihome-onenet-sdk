# 钛极智能呼叫网关TiHome微信小程序SDK

本例程提供了微信小程序下的相关API例程，方便用户通过微信小程序快速访问设备。

在使用时，请使用微信开发工具打开工程，并将您的OneNET账号userid及access key填写到app.js代码中。 

使用前先使用 npm install 安装依赖的querystring， base64-arraybuffer组件， 如何在微信开发工具中引用npm包， 请参考如下链接：

https://developers.weixin.qq.com/community/develop/article/doc/0008aecec4c9601e750be048d51c13

